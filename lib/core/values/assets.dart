abstract class ImageAssets {
  static const imagePath = 'assets/images';
  static const launcherIcon = '$imagePath/launcher_icon.png';
  static const splashIcon = '$imagePath/splash_icon.png';
}

abstract class IconAssets {
  static const iconPath = 'assets/icons';
  static const mottomastIcon = '$iconPath/mottomast.svg';
}
