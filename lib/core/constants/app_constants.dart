abstract class AppConstants {
  static const baseApiUrl = '$baseUrl/api/v1';
  static const baseUrl = 'https://mottomast-backend.herokuapp.com';
}
