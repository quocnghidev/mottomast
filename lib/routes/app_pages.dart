import 'package:get/get.dart';
import 'package:mottomast/modules/create_channel_chat/create_channel_chat_binding.dart';
import 'package:mottomast/modules/create_channel_chat/create_channel_chat_screen.dart';
import 'package:mottomast/modules/create_direct_chat/create_direct_chat_binding.dart';
import 'package:mottomast/modules/create_direct_chat/create_direct_chat_screen.dart';
import 'package:mottomast/modules/home/home_binding.dart';
import 'package:mottomast/modules/home/home_screen.dart';
import 'package:mottomast/modules/login/login_binding.dart';
import 'package:mottomast/modules/login/login_screen.dart';
import 'package:mottomast/modules/register/register_binding.dart';
import 'package:mottomast/modules/register/register_screen.dart';
import 'package:mottomast/modules/splash/splash_binding.dart';
import 'package:mottomast/modules/splash/splash_screen.dart';
import 'package:mottomast/modules/view_member/view_member_binding.dart';
import 'package:mottomast/modules/view_member/view_member_screen.dart';
import 'package:mottomast/modules/view_pinned/view_pinned_binding.dart';
import 'package:mottomast/modules/view_pinned/view_pinned_screen.dart';
import 'package:mottomast/modules/view_room_info/view_room_info_binding.dart';
import 'package:mottomast/modules/view_room_info/view_room_info_screen.dart';
import 'package:mottomast/routes/global_middleware.dart';
import 'package:mottomast/routes/routes.dart';

abstract class AppPages {
  static final routes = [
    GetPage(
      name: Routes.SPLASH,
      page: () => const SplashScreen(),
      binding: SplashBinding(),
    ),
    GetPage(
      name: Routes.LOGIN,
      page: () => const LoginScreen(),
      binding: LoginBinding(),
    ),
    GetPage(
      name: Routes.REGISTER,
      page: () => const RegisterScreen(),
      binding: RegisterBinding(),
    ),
    GetPage(
      name: Routes.HOME,
      page: () => const HomeScreen(),
      binding: HomeBinding(),
      middlewares: [
        GlobalMiddleware(),
      ],
    ),
    GetPage(
      name: Routes.CREATE_DIRECT_CHAT,
      page: () => const CreateDirectChatScreen(),
      binding: CreateDirectChatBinding(),
      middlewares: [
        GlobalMiddleware(),
      ],
    ),
    GetPage(
      name: Routes.CREATE_CHANNEL_CHAT,
      page: () => const CreateChannelChatScreen(),
      binding: CreateChannelChatBinding(),
      middlewares: [
        GlobalMiddleware(),
      ],
    ),
    GetPage(
      name: Routes.VIEW_ROOM_INFO,
      page: () => const ViewRoomInfoScreen(),
      binding: ViewRoomInfoBinding(),
      middlewares: [
        GlobalMiddleware(),
      ],
    ),
    GetPage(
      name: Routes.VIEW_MEMBER,
      page: () => const ViewMemberScreen(),
      binding: ViewMemberBinding(),
      middlewares: [
        GlobalMiddleware(),
      ],
    ),
    GetPage(
      name: Routes.VIEW_PINNED,
      page: () => const ViewPinnedScreen(),
      binding: ViewPinnedBinding(),
      middlewares: [
        GlobalMiddleware(),
      ],
    ),
  ];
}
