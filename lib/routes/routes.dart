// ignore_for_file: constant_identifier_names

abstract class Routes {
  static const CREATE_CHANNEL_CHAT = '/create_channel_chat';
  static const CREATE_DIRECT_CHAT = '/create_direct_chat';
  static const HOME = '/home';
  static const LOGIN = '/login';
  static const REGISTER = '/register';
  static const SPLASH = '/splash';
  static const VIEW_MEMBER = '/view_member';
  static const VIEW_PINNED = '/view_pinned';
  static const VIEW_ROOM_INFO = '/view_room_info';
}
