import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mottomast/core/constants/storage_keys.dart';
import 'package:mottomast/routes/routes.dart';

class GlobalMiddleware extends GetMiddleware {
  final box = GetStorage();

  @override
  RouteSettings? redirect(String? route) {
    String? token = box.read(StorageKeys.accessToken);
    return token == null || token.isEmpty
        ? const RouteSettings(name: Routes.LOGIN)
        : null;
  }
}
