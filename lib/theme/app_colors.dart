import 'package:flutter/material.dart';

abstract class AppColors {
  static const errorText = Color(0xFFCB4E52);
  static const info = Color(0xFF2388D6);
  static const primaryColor = Color(0xFF1A2A4C);
  static const primaryText = Color(0xFF1A1A1A);
  static const secondaryColor = Color(0xFF1F325C);
  static const secondaryText = Color(0xFF757575);
  static const statusColor = Color(0xFF131F39);
  static const success = Color(0xFF28A745);
}
