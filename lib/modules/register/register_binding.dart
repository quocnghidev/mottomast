import 'package:get/get.dart';
import 'package:mottomast/modules/register/register_controller.dart';

class RegisterBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => RegisterController(apiRepository: Get.find()));
  }
}
