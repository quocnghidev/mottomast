import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:mottomast/core/values/assets.dart';
import 'package:mottomast/modules/register/register_controller.dart';
import 'package:mottomast/theme/app_colors.dart';

class RegisterScreen extends GetView<RegisterController> {
  const RegisterScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.focusScope?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: Text('create_account'.tr),
          centerTitle: true,
        ),
        body: SingleChildScrollView(
          child: Column(
            children: [
              const SizedBox(height: 56),
              SizedBox(
                width: double.infinity,
                child: SvgPicture.asset(
                  IconAssets.mottomastIcon,
                  color: AppColors.primaryColor,
                  height: 76,
                ),
              ),
              const SizedBox(height: 28),
              const Text(
                'Mottomast',
                style: TextStyle(
                  fontSize: 24,
                  color: AppColors.secondaryText,
                ),
              ),
              const SizedBox(height: 24),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 16),
                child: Column(
                  children: [
                    Obx(
                      () => controller.registerError.value.isNotEmpty
                          ? Column(
                              children: [
                                Text(
                                  controller.registerError.value,
                                  style: const TextStyle(
                                    fontSize: 12,
                                    color: AppColors.errorText,
                                  ),
                                ),
                                const SizedBox(height: 16),
                              ],
                            )
                          : const SizedBox(),
                    ),
                    Obx(
                      () => controller.registerSuccess.value.isNotEmpty
                          ? Column(
                              children: [
                                Text(
                                  controller.registerSuccess.value,
                                  style: const TextStyle(
                                    fontSize: 12,
                                    color: AppColors.success,
                                  ),
                                ),
                                const SizedBox(height: 16),
                              ],
                            )
                          : const SizedBox(),
                    ),
                    TextField(
                      controller: controller.fullNameField,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 10,
                        ),
                        border: const OutlineInputBorder(),
                        hintText: 'full_name'.tr,
                      ),
                    ),
                    const SizedBox(height: 16),
                    TextField(
                      controller: controller.emailField,
                      decoration: const InputDecoration(
                        contentPadding: EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 10,
                        ),
                        border: OutlineInputBorder(),
                        hintText: 'Email',
                      ),
                    ),
                    const SizedBox(height: 16),
                    TextField(
                      controller: controller.passwordField,
                      obscureText: true,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 10,
                        ),
                        border: const OutlineInputBorder(),
                        hintText: 'password'.tr,
                      ),
                    ),
                    const SizedBox(height: 16),
                    TextField(
                      controller: controller.confirmField,
                      obscureText: true,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.symmetric(
                          vertical: 10,
                          horizontal: 10,
                        ),
                        border: const OutlineInputBorder(),
                        hintText: 'confirm_password'.tr,
                      ),
                    ),
                    const SizedBox(height: 16),
                    SizedBox(
                      width: double.infinity,
                      child: ElevatedButton(
                        onPressed: () {
                          Get.focusScope?.unfocus();
                          controller.register();
                        },
                        style: ElevatedButton.styleFrom(
                          padding: const EdgeInsets.symmetric(vertical: 16),
                        ),
                        child: Obx(
                          () => controller.isLoading.value
                              ? const SizedBox(
                                  height: 20,
                                  width: 20,
                                  child: CircularProgressIndicator(
                                    color: Colors.white,
                                  ),
                                )
                              : Text(
                                  'create_account'.tr,
                                  style: const TextStyle(
                                    fontSize: 16,
                                  ),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              const SizedBox(height: 16),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'already_have_account'.tr,
                    style: const TextStyle(
                      fontSize: 16,
                      color: AppColors.secondaryText,
                    ),
                  ),
                  TextButton(
                    onPressed: () => Get.back(),
                    style: TextButton.styleFrom(
                      padding: const EdgeInsets.all(2),
                      foregroundColor: AppColors.info,
                      minimumSize: Size.zero,
                      tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                      textStyle: const TextStyle(
                        fontSize: 16,
                      ),
                    ),
                    child: Text('login_now'.tr),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
