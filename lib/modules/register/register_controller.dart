import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/models/base_error/base_error.dart';
import 'package:mottomast/models/register_body/register_body.dart';

class RegisterController extends GetxController {
  RegisterController({required this.apiRepository});

  final ApiRepository apiRepository;
  final confirmField = TextEditingController();
  final emailField = TextEditingController();
  final fullNameField = TextEditingController();
  RxBool isLoading = false.obs;
  final passwordField = TextEditingController();
  RxString registerError = ''.obs;
  RxString registerSuccess = ''.obs;

  @override
  void onClose() {
    emailField.dispose();
    fullNameField.dispose();
    passwordField.dispose();
    confirmField.dispose();
    super.onClose();
  }

  Future<void> register() async {
    registerError.value = '';
    registerSuccess.value = '';

    final fullName = fullNameField.text.trim();
    final email = emailField.text.trim();
    final password = passwordField.text.trim();
    final confirm = confirmField.text.trim();

    if (fullName.isEmpty ||
        email.isEmpty ||
        password.isEmpty ||
        confirm.isEmpty) {
      registerError.value = 'Vui lòng nhập đầy đủ tất cả các trường';
      return;
    }

    if (password.length < 8) {
      registerError.value = 'Mật khẩu yêu cầu ít nhất 8 ký tự';
      return;
    }

    if (password != confirm) {
      registerError.value = 'Mật khẩu nhập lại không khớp';
      return;
    }

    isLoading.value = true;

    final res = await apiRepository.register(
      RegisterBody(
        fullName: fullName,
        email: email,
        password: password,
      ),
    );

    if (res.statusCode == HttpStatus.created) {
      registerSuccess.value = 'Đăng ký thành công. Bạn đã có thể đăng nhập';
    } else {
      final response = BaseError.fromJson(res.body);
      switch (response.error) {
        case 'Email Existed':
          registerError.value = 'Địa chỉ Email này đã được sử dụng';
          break;
        case 'Invalid Parameter':
          registerError.value = 'Địa chỉ Email không hợp lệ';
          break;
        default:
          registerError.value = 'Có lỗi xảy ra, vui lòng thử lại sau';
          break;
      }
    }

    isLoading.value = false;
  }
}
