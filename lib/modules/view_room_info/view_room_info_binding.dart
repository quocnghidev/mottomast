import 'package:get/get.dart';
import 'package:mottomast/modules/view_room_info/view_room_info_controller.dart';

class ViewRoomInfoBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ViewRoomInfoController(
        apiRepository: Get.find(), homeController: Get.find()));
  }
}
