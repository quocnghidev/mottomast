// ignore_for_file: invalid_use_of_protected_member

import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/models/list_message_response/message_response.dart';
import 'package:mottomast/models/list_pinned_response/list_pinned_response.dart';
import 'package:mottomast/modules/home/home_controller.dart';

class ViewRoomInfoController extends GetxController {
  ViewRoomInfoController({
    required this.apiRepository,
    required this.homeController,
  });

  final ApiRepository apiRepository;
  final HomeController homeController;
  final listPinned = <MessageResponse>[].obs;

  @override
  void onInit() {
    super.onInit();
    getListPinned();
  }

  Future<void> getListPinned() async {
    final res =
        await apiRepository.getListPinned(homeController.activeRoomId.value);
    if (res.statusCode != HttpStatus.ok) return;
    final response = ListPinnedResponse.fromJson(res.body);
    listPinned.value = response.data!;
  }
}
