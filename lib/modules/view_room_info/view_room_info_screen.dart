// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mottomast/modules/view_room_info/view_room_info_controller.dart';
import 'package:mottomast/routes/routes.dart';
import 'package:mottomast/theme/app_colors.dart';

class ViewRoomInfoScreen extends GetView<ViewRoomInfoController> {
  const ViewRoomInfoScreen({super.key});

  @override
  Widget build(BuildContext context) {
    final homeController = controller.homeController;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Thông tin'),
        centerTitle: true,
      ),
      body: Column(
        children: [
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color(0xFFe6e7e9),
                ),
              ),
            ),
            child: Row(
              children: [
                Obx(
                  () => homeController.currentRoom.value!.avatar.isEmpty
                      ? const Icon(
                          Icons.language_rounded,
                          color: AppColors.secondaryText,
                          size: 48,
                        )
                      : CircleAvatar(
                          radius: 24,
                          backgroundColor: AppColors.primaryColor,
                          child: Obx(
                            () => Text(
                              homeController.currentRoom.value!.avatar,
                              style: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 20,
                                height: 1,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                ),
                const SizedBox(width: 16),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Obx(
                      () => Text(
                        homeController.currentRoom.value?.name ?? '',
                        style: const TextStyle(
                          fontSize: 18,
                          color: AppColors.primaryText,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
          const SizedBox(height: 30),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color(0xFFe6e7e9),
                ),
                top: BorderSide(
                  width: 1,
                  color: Color(0xFFe6e7e9),
                ),
              ),
            ),
            child: GestureDetector(
              onTap: () => Get.toNamed(Routes.VIEW_MEMBER),
              child: Row(
                children: [
                  const Icon(
                    Icons.people_alt_rounded,
                    size: 16,
                    color: AppColors.primaryText,
                  ),
                  const SizedBox(width: 8),
                  const Expanded(
                    child: Text(
                      'Xem thành viên',
                      style: TextStyle(
                        color: AppColors.primaryText,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Obx(
                    () => Text(
                      homeController.userInfo.value.rooms!
                          .firstWhere(
                              (e) => e.id == homeController.activeRoomId.value)
                          .users!
                          .length
                          .toString(),
                      style: const TextStyle(
                        color: AppColors.primaryText,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  const Icon(
                    Icons.keyboard_arrow_right_rounded,
                    size: 20,
                    color: AppColors.primaryText,
                  ),
                ],
              ),
            ),
          ),
          Container(
            padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 16),
            decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(
                bottom: BorderSide(
                  width: 1,
                  color: Color(0xFFe6e7e9),
                ),
                top: BorderSide(
                  width: 1,
                  color: Color(0xFFe6e7e9),
                ),
              ),
            ),
            child: GestureDetector(
              onTap: () => Get.toNamed(Routes.VIEW_PINNED),
              child: Row(
                children: [
                  const Icon(
                    Icons.push_pin_rounded,
                    size: 16,
                    color: AppColors.primaryText,
                  ),
                  const SizedBox(width: 8),
                  const Expanded(
                    child: Text(
                      'Xem tin nhắn đã ghim',
                      style: TextStyle(
                        color: AppColors.primaryText,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  Obx(
                    () => Text(
                      controller.listPinned.value.length.toString(),
                      style: const TextStyle(
                        color: AppColors.primaryText,
                        fontSize: 16,
                      ),
                    ),
                  ),
                  const SizedBox(width: 8),
                  const Icon(
                    Icons.keyboard_arrow_right_rounded,
                    size: 20,
                    color: AppColors.primaryText,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
