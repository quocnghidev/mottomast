import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mottomast/modules/home/home_controller.dart';
import 'package:mottomast/theme/app_colors.dart';

class HomeMenuItem extends GetView<HomeController> {
  const HomeMenuItem({
    Key? key,
    required this.room,
  }) : super(key: key);

  final RoomItem room;

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Obx(
          () => Container(
            color: controller.activeRoomId.value == room.id
                ? const Color(0xFF35476d)
                : null,
            padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
            child: Row(
              children: [
                room.avatar.isEmpty
                    ? const Icon(
                        Icons.language_rounded,
                        color: Colors.white,
                        size: 32,
                      )
                    : CircleAvatar(
                        radius: 16,
                        backgroundColor: AppColors.primaryColor,
                        child: Text(
                          room.avatar,
                          style: const TextStyle(
                            fontWeight: FontWeight.w500,
                            height: 1,
                            color: Colors.white,
                          ),
                        ),
                      ),
                const SizedBox(width: 16),
                Text(
                  room.name,
                  style: TextStyle(
                    fontSize: 16,
                    fontWeight: FontWeight.w500,
                    color: controller.activeRoomId.value == room.id
                        ? Colors.white
                        : room.unreadCount != 0
                            ? Colors.white
                            : const Color(0xFFa6adbf),
                  ),
                ),
                const Expanded(child: SizedBox()),
                if (room.unreadCount != 0)
                  CircleAvatar(
                    radius: 10,
                    backgroundColor: Colors.white,
                    child: FittedBox(
                      child: Padding(
                        padding: const EdgeInsets.all(6),
                        child: Text(
                          '${room.unreadCount}',
                          style: const TextStyle(
                            height: 1,
                            fontWeight: FontWeight.bold,
                            color: AppColors.primaryColor,
                          ),
                        ),
                      ),
                    ),
                  ),
              ],
            ),
          ),
        ),
        Positioned(
          top: 0,
          left: 0,
          bottom: 0,
          child: Obx(
            () => Container(
              width: 6,
              color: controller.activeRoomId.value == room.id
                  ? const Color(0xFF5d89ea)
                  : null,
            ),
          ),
        ),
        Positioned.fill(
          child: GestureDetector(
            onTap: () {
              if (!(controller.activeRoomId.value == room.id)) {
                controller.prevRoomId = controller.activeRoomId.value;
                controller.activeRoomId.value = room.id;
              }
            },
            child: Container(
              width: double.infinity,
              height: double.infinity,
              color: Colors.transparent,
            ),
          ),
        ),
      ],
    );
  }
}
