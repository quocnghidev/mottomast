// ignore_for_file: invalid_use_of_protected_member, depend_on_referenced_packages

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:expandable/expandable.dart';
import 'package:file_picker/file_picker.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';
import 'package:mottomast/modules/home/chat_l10n.dart';
import 'package:mottomast/modules/home/chat_theme.dart';
import 'package:mottomast/modules/home/home_controller.dart';
import 'package:mottomast/modules/home/home_menu_item.dart';
import 'package:mottomast/modules/home/models/emoji_enlargement_behavior.dart';
import 'package:mottomast/modules/home/models/send_button_visibility_mode.dart';
import 'package:mottomast/modules/home/widgets/chat.dart';
import 'package:mottomast/modules/home/widgets/input/input.dart';
import 'package:mottomast/routes/routes.dart';
import 'package:mottomast/theme/app_colors.dart';

class HomeScreen extends GetView<HomeController> {
  const HomeScreen({super.key});

  void _handlePreviewDataFetched(
    TextMessage message,
    PreviewData previewData,
  ) {
    final index = controller.messages.value
        .indexWhere((element) => element.id == message.id);
    final updatedMessage =
        (controller.messages.value[index] as TextMessage).copyWith(
      previewData: previewData,
    );

    controller.messages[index] = updatedMessage;
  }

  void _handleImageSelection() async {
    final result = await ImagePicker().pickImage(
      imageQuality: 100,
      maxWidth: 1920,
      source: ImageSource.gallery,
    );
    if (result != null) {
      final bytes = await result.readAsBytes();
      controller.sendChatFile(
        pathTofile: result.path,
        fileName:
            '${DateTime.now().millisecondsSinceEpoch}.${result.name.split('.').last}',
        fileSize: bytes.length,
        type: 1,
      );
    }
  }

  void _handleFileSelection() async {
    final result = await FilePicker.platform.pickFiles(
      type: FileType.any,
    );

    if (result != null && result.files.single.path != null) {
      controller.sendChatFile(
        pathTofile: result.files.single.path!,
        fileName:
            '${DateTime.now().millisecondsSinceEpoch}.${result.files.single.name.split('.').last}',
        fileSize: result.files.single.size,
        type: 2,
      );
    }
  }

  void _handleMessageTap(BuildContext _, Message message) async {
    if (message is FileMessage) {
      var localPath = message.uri;

      if (message.uri.startsWith('http')) {
        try {
          // Update tapped file message to show loading spinner
          final index = controller.messages.value
              .indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (controller.messages.value[index] as FileMessage).copyWith(
            isLoading: true,
          );

          controller.messages[index] = updatedMessage;

          final client = http.Client();
          final request = await client.get(Uri.parse(message.uri));
          final bytes = request.bodyBytes;
          final documentsDir = (await getApplicationDocumentsDirectory()).path;
          localPath = '$documentsDir/${message.name}';

          if (!File(localPath).existsSync()) {
            final file = File(localPath);
            await file.writeAsBytes(bytes);
          }
        } finally {
          // In case of error or success, reset loading spinner
          final index = controller.messages.value
              .indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (controller.messages.value[index] as FileMessage).copyWith(
            isLoading: null,
          );
          controller.messages[index] = updatedMessage;
        }
      }

      await OpenFilex.open(localPath);
    }
  }

  void _handleAttachmentPressed() {
    Get.bottomSheet(
      Container(
        width: double.infinity,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          ),
          boxShadow: [
            BoxShadow(
              color: Colors.white.withOpacity(0.10),
              offset: const Offset(0, -2),
              blurRadius: 7,
            ),
          ],
        ),
        child: SafeArea(
          top: false,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            children: [
              TextButton(
                onPressed: () {
                  Get.back();
                  _handleImageSelection();
                },
                child: const Align(
                  alignment: AlignmentDirectional.center,
                  child: Text(
                    'Chọn ảnh',
                    style: TextStyle(),
                  ),
                ),
              ),
              TextButton(
                onPressed: () {
                  Get.back();
                  _handleFileSelection();
                },
                child: const Align(
                  alignment: AlignmentDirectional.center,
                  child: Text(
                    'Đính kèm tệp',
                    style: TextStyle(),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: GestureDetector(
        onTap: () => Get.focusScope?.unfocus(),
        child: Scaffold(
          appBar: AppBar(
            title: Obx(
              () => GestureDetector(
                onTap: () => Get.toNamed(Routes.VIEW_ROOM_INFO),
                child: Row(
                  children: [
                    Text(controller.currentRoom.value?.name ?? ''),
                    const Icon(Icons.expand_more_rounded),
                  ],
                ),
              ),
            ),
            centerTitle: true,
            leading: Builder(
              builder: (context) => IconButton(
                icon: const Icon(Icons.menu),
                onPressed: () => Scaffold.of(context).openDrawer(),
              ),
            ),
            actions: [
              Builder(
                builder: (context) => IconButton(
                  icon: const Icon(Icons.more_vert_rounded),
                  onPressed: () => Scaffold.of(context).openEndDrawer(),
                ),
              ),
            ],
          ),
          drawer: Drawer(
            child: Container(
              color: AppColors.secondaryColor,
              child: SingleChildScrollView(
                key: const PageStorageKey<String>('drawer'),
                physics: const AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: [
                    const SizedBox(height: 12),
                    ExpandablePanel(
                      controller: controller.channelExpandableController,
                      theme: const ExpandableThemeData(
                        iconPlacement: ExpandablePanelIconPlacement.left,
                        iconColor: Color(0xFF76849f),
                        expandIcon: Icons.expand_more_rounded,
                        collapseIcon: Icons.expand_more_rounded,
                        headerAlignment: ExpandablePanelHeaderAlignment.center,
                        iconPadding: EdgeInsets.only(bottom: 6),
                      ),
                      header: Padding(
                        padding: const EdgeInsets.only(bottom: 6),
                        child: Row(
                          children: [
                            const Text(
                              'TIN NHẮN KÊNH CHAT',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF76849f),
                              ),
                            ),
                            const Expanded(child: SizedBox()),
                            GestureDetector(
                              onTap: () =>
                                  Get.toNamed(Routes.CREATE_CHANNEL_CHAT),
                              child: const Icon(
                                Icons.add,
                                size: 20,
                                color: Color(0xFF76849f),
                              ),
                            ),
                            const SizedBox(width: 16),
                          ],
                        ),
                      ),
                      collapsed: const SizedBox(),
                      expanded: Obx(
                        () => Column(
                          children: [
                            ...List.generate(
                              controller.listGroupChatRoom.length,
                              (index) => HomeMenuItem(
                                room: controller.listGroupChatRoom[index],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 12),
                    ExpandablePanel(
                      controller: controller.privateExpandableController,
                      theme: const ExpandableThemeData(
                        iconPlacement: ExpandablePanelIconPlacement.left,
                        iconColor: Color(0xFF76849f),
                        expandIcon: Icons.expand_more_rounded,
                        collapseIcon: Icons.expand_more_rounded,
                        headerAlignment: ExpandablePanelHeaderAlignment.center,
                        iconPadding: EdgeInsets.only(bottom: 6),
                      ),
                      header: Padding(
                        padding: const EdgeInsets.only(bottom: 6),
                        child: Row(
                          children: [
                            const Text(
                              'TIN NHẮN TRỰC TIẾP',
                              style: TextStyle(
                                fontWeight: FontWeight.w500,
                                color: Color(0xFF76849f),
                              ),
                            ),
                            const Expanded(child: SizedBox()),
                            GestureDetector(
                              onTap: () =>
                                  Get.toNamed(Routes.CREATE_DIRECT_CHAT),
                              child: const Icon(
                                Icons.add,
                                size: 20,
                                color: Color(0xFF76849f),
                              ),
                            ),
                            const SizedBox(width: 16),
                          ],
                        ),
                      ),
                      collapsed: const SizedBox(),
                      expanded: Obx(
                        () => Column(
                          children: [
                            ...List.generate(
                              controller.listChatRoom.length,
                              (index) => HomeMenuItem(
                                room: controller.listChatRoom[index],
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(height: 32),
                  ],
                ),
              ),
            ),
          ),
          endDrawer: Drawer(
            child: Container(
              color: const Color(0xFFf9f9f9),
              child: Column(
                children: [
                  Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 16),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        bottom: BorderSide(
                          width: 1,
                          color: Color(0xFFe6e7e9),
                        ),
                      ),
                    ),
                    child: Row(
                      children: [
                        CircleAvatar(
                          radius: 24,
                          backgroundColor: AppColors.primaryColor,
                          child: Obx(
                            () => Text(
                              controller.userInfo.value.fullName
                                      ?.split(' ')
                                      .last[0] ??
                                  '',
                              style: const TextStyle(
                                fontWeight: FontWeight.w500,
                                fontSize: 20,
                                height: 1,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        const SizedBox(width: 16),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Obx(
                              () => Text(
                                '@${controller.userInfo.value.nickName ?? ""}',
                                style: const TextStyle(
                                  fontSize: 16,
                                  color: AppColors.primaryText,
                                ),
                              ),
                            ),
                            const SizedBox(height: 8),
                            Obx(
                              () => Text(
                                controller.userInfo.value.fullName ?? '',
                                style: const TextStyle(
                                  fontSize: 16,
                                  color: AppColors.secondaryText,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 30),
                  Container(
                    padding: const EdgeInsets.symmetric(
                        vertical: 20, horizontal: 16),
                    decoration: const BoxDecoration(
                      color: Colors.white,
                      border: Border(
                        bottom: BorderSide(
                          width: 1,
                          color: Color(0xFFe6e7e9),
                        ),
                        top: BorderSide(
                          width: 1,
                          color: Color(0xFFe6e7e9),
                        ),
                      ),
                    ),
                    child: GestureDetector(
                      onTap: () => controller.logout(),
                      child: Row(
                        children: const [
                          Icon(
                            Icons.logout_rounded,
                            size: 16,
                            color: AppColors.errorText,
                          ),
                          SizedBox(width: 8),
                          Expanded(
                            child: Text(
                              'Đăng xuất',
                              style: TextStyle(
                                color: AppColors.errorText,
                                fontSize: 16,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          body: Obx(
            () => Chat(
              onEndReached: () async {
                if (!controller.canLoadMore.value) return;
                if (controller.isLoadingMore.value) return;
                controller.loadMoreMessage();
              },
              onEndReachedThreshold: 1.0,
              inputOptions: const InputOptions(
                sendButtonVisibilityMode: SendButtonVisibilityMode.always,
              ),
              emojiEnlargementBehavior: EmojiEnlargementBehavior.never,
              theme: const DefaultChatTheme(
                inputTextColor: AppColors.primaryText,
                inputBackgroundColor: Color(0xFFf2f3f5),
                inputMargin: EdgeInsets.all(12),
                inputPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 6),
                inputBorderRadius: BorderRadius.all(Radius.circular(40)),
                primaryColor: AppColors.secondaryColor,
              ),
              showUserAvatars: true,
              showUserNames: true,
              dateLocale: 'vi_VN',
              l10n: const ChatL10nEn(
                inputPlaceholder: 'Nhắn tin',
                emptyChatPlaceholder: 'Không có tin nhắn nào hiển thị',
                sendButtonAccessibilityLabel: 'Gửi',
                unreadMessagesLabel: 'Tin nhắn chưa đọc',
                fileButtonAccessibilityLabel: 'Tệp',
                attachmentButtonAccessibilityLabel: 'Hình ảnh',
              ),
              messages: controller.messages.value,
              onSendPressed: (msg) => controller.sendChatText(msg.text),
              onAttachmentPressed: _handleAttachmentPressed,
              onMessageTap: _handleMessageTap,
              onPreviewDataFetched: _handlePreviewDataFetched,
              user: User(
                id: '${controller.userInfo.value.id}',
                firstName: controller.userInfo.value.fullName,
              ),
              onMessageLongPress: (context, msg) {
                Get.bottomSheet(
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(20),
                        topRight: Radius.circular(20),
                      ),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.white.withOpacity(0.10),
                          offset: const Offset(0, -2),
                          blurRadius: 7,
                        ),
                      ],
                    ),
                    child: SafeArea(
                      top: false,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          TextButton(
                            onPressed: () {
                              controller.updatePinned(
                                  int.parse(msg.id),
                                  !controller.listMsgTmp
                                      .firstWhere((element) =>
                                          element.id == int.parse(msg.id))
                                      .isPinned!);
                              Get.back();
                            },
                            child: Align(
                              alignment: AlignmentDirectional.center,
                              child: Text(
                                controller.listMsgTmp
                                        .firstWhere((element) =>
                                            element.id == int.parse(msg.id))
                                        .isPinned!
                                    ? 'Bỏ ghim tin nhắn này'
                                    : 'Ghim tin nhắn này',
                                style: const TextStyle(),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
            ),
          ),
        ),
      ),
    );
  }
}
