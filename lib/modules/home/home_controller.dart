// ignore_for_file: invalid_use_of_protected_member

import 'dart:async';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:expandable/expandable.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:get_storage/get_storage.dart' hide Data;
import 'package:mottomast/models/chat_text_body/chat_text_body.dart';
import 'package:mottomast/models/chat_text_response/chat_text_response.dart';
import 'package:mottomast/models/list_message_response/message_response.dart';
import 'package:socket_io_client/socket_io_client.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/core/constants/app_constants.dart';
import 'package:mottomast/core/constants/storage_keys.dart';
import 'package:mottomast/models/list_message_response/list_message_response.dart';
import 'package:mottomast/models/socket_response/room_created.dart';
import 'package:mottomast/models/socket_response/unread_count_changed.dart';
import 'package:mottomast/models/user_info_response/data.dart';
import 'package:mottomast/models/user_info_response/user_info_response.dart';
import 'package:mottomast/routes/routes.dart';

class HomeController extends FullLifeCycleController with FullLifeCycleMixin {
  HomeController({required this.apiRepository});

  var activeRoomId = 1.obs;
  final ApiRepository apiRepository;
  final box = GetStorage();
  var canLoadMore = false.obs;
  var channelExpandableController = ExpandableController(initialExpanded: true);
  var connectivity = Connectivity();
  var currentPage = 1.obs;
  var currentRoom = Rxn<RoomItem>();
  var isLoadingMore = false.obs;
  var listChatRoom = <RoomItem>[].obs;
  var listGroupChatRoom = <RoomItem>[].obs;
  var listMsgTmp = <MessageResponse>[];
  var messages = <types.Message>[].obs;
  int? prevRoomId;
  final privateExpandableController =
      ExpandableController(initialExpanded: true);

  late final Socket socket;
  late StreamSubscription streamSubscription;
  var userInfo = Data().obs;

  @override
  void onClose() {
    channelExpandableController.dispose();
    privateExpandableController.dispose();
    streamSubscription.cancel();
    socket.disconnect();
    socket.dispose();
    super.onClose();
  }

  @override
  void onDetached() {}

  @override
  void onInactive() {}

  @override
  void onInit() {
    super.onInit();
    initSocket();
    getConnectivityType();
    streamSubscription =
        connectivity.onConnectivityChanged.listen(updateConnectState);
    initializeListeners();
    getUserInfo();
    getListMessage(currentPage.value);
  }

  @override
  void onPaused() {}

  @override
  void onResumed() {
    socket.connect();
  }

  Future<void> sendChatFile({
    required String pathTofile,
    required String fileName,
    required num fileSize,
    required int type,
  }) async {
    final res = await apiRepository.sendChatFile(
      FormData(
        {
          'file': MultipartFile(File(pathTofile), filename: fileName),
          'file_name': fileName,
          'file_size': fileSize,
          'room_id': activeRoomId.value,
          'type': type,
        },
      ),
    );
    if (res.statusCode != HttpStatus.ok) return;
    final response = ChatTextResponse.fromJson(res.body);
    messages.insert(
      0,
      response.data!.type == 1
          ? types.ImageMessage(
              author: types.User(
                  id: '${response.data!.userId}',
                  firstName: response.data!.user!.fullName),
              id: '${response.data!.id}',
              uri: '${AppConstants.baseUrl}/${response.data!.fileName!}',
              createdAt: response.data!.createdAt!.millisecondsSinceEpoch,
              name: response.data!.fileName!,
              size: response.data!.fileSize!,
            )
          : types.FileMessage(
              author: types.User(
                  id: '${response.data!.userId}',
                  firstName: response.data!.user!.fullName),
              id: '${response.data!.id}',
              uri: '${AppConstants.baseUrl}/${response.data!.fileName!}',
              createdAt: response.data!.createdAt!.millisecondsSinceEpoch,
              name: response.data!.fileName!,
              size: response.data!.fileSize!,
            ),
    );
  }

  Future<void> loadMoreMessage() async {
    isLoadingMore.value = true;
    currentPage.value = currentPage.value + 1;
    final res = await apiRepository.getListMessage(
        activeRoomId.value, currentPage.value);
    if (res.statusCode != HttpStatus.ok) {
      currentPage.value = currentPage.value - 1;
      isLoadingMore.value = false;
      return;
    }
    final response = ListMessageResponse.fromJson(res.body);
    canLoadMore.value = response.data!.meta!.next != null;
    var filtered = response.data!.data!;
    for (var msg in messages.value) {
      filtered = filtered.where((e) => e.id != int.parse(msg.id)).toList();
    }
    listMsgTmp.addAll(List.from(filtered));
    messages.addAll(filtered.map((e) {
      if (e.type == 1) {
        return types.ImageMessage(
          author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
          id: '${e.id}',
          name: e.fileName!,
          size: e.fileSize!,
          uri: '${AppConstants.baseUrl}/${e.fileName!}',
          createdAt: e.createdAt!.millisecondsSinceEpoch,
        );
      }
      if (e.type == 2) {
        return types.FileMessage(
          author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
          id: '${e.id}',
          name: e.fileName!,
          size: e.fileSize!,
          uri: '${AppConstants.baseUrl}/${e.fileName!}',
          createdAt: e.createdAt!.millisecondsSinceEpoch,
        );
      }
      return types.TextMessage(
        author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
        id: '${e.id}',
        text: e.content!,
        createdAt: e.createdAt!.millisecondsSinceEpoch,
      );
    }).toList());
    isLoadingMore.value = false;
  }

  Future<void> getListMessage(int page) async {
    final res = await apiRepository.getListMessage(activeRoomId.value, page);
    if (res.statusCode != HttpStatus.ok) return;
    final response = ListMessageResponse.fromJson(res.body);
    canLoadMore.value = response.data!.meta!.next != null;
    listMsgTmp = List.from(response.data!.data!);
    messages.value = response.data?.data?.map((e) {
          if (e.type == 1) {
            return types.ImageMessage(
              author:
                  types.User(id: '${e.userId}', firstName: e.user!.fullName),
              id: '${e.id}',
              name: e.fileName!,
              size: e.fileSize!,
              uri: '${AppConstants.baseUrl}/${e.fileName!}',
              createdAt: e.createdAt!.millisecondsSinceEpoch,
            );
          }
          if (e.type == 2) {
            return types.FileMessage(
              author:
                  types.User(id: '${e.userId}', firstName: e.user!.fullName),
              id: '${e.id}',
              name: e.fileName!,
              size: e.fileSize!,
              uri: '${AppConstants.baseUrl}/${e.fileName!}',
              createdAt: e.createdAt!.millisecondsSinceEpoch,
            );
          }
          return types.TextMessage(
            author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
            id: '${e.id}',
            text: e.content!,
            createdAt: e.createdAt!.millisecondsSinceEpoch,
          );
        }).toList() ??
        [];
  }

  void joinRoom() {
    socket.emit('JOIN_ROOM', {
      'prevRoomId': prevRoomId,
      'currentRoomId': activeRoomId.value,
    });
  }

  Future<void> getConnectivityType() async {
    late ConnectivityResult connectivityResult;
    try {
      connectivityResult = await (connectivity.checkConnectivity());
    } on PlatformException catch (e) {
      if (kDebugMode) {
        print(e);
      }
    }
    return updateConnectState(connectivityResult);
  }

  updateConnectState(ConnectivityResult result) {
    switch (result) {
      case ConnectivityResult.wifi:
        socket.connect();
        break;
      case ConnectivityResult.mobile:
        socket.connect();
        break;
      case ConnectivityResult.none:
        Fluttertoast.showToast(msg: 'Kết nối thất bại, vui lòng kiểm tra mạng');
        socket.connect();
        break;
      default:
        break;
    }
  }

  void initSocket() {
    String? token = box.read(StorageKeys.accessToken);
    socket = io(
        '${AppConstants.baseUrl}/cable',
        OptionBuilder().setTransports(['websocket']).setExtraHeaders({
          'authorization': token ?? '',
        }).build());
    socket.onConnect((_) => joinRoom());
    socket.onError((_) {
      Fluttertoast.showToast(msg: 'Kết nối thất bại, vui lòng kiểm tra mạng');
      socket.connect();
    });
    socket.onConnectError((_) {
      Fluttertoast.showToast(msg: 'Kết nối thất bại, vui lòng kiểm tra mạng');
      socket.connect();
    });
    socket.onReconnectFailed((_) {
      Fluttertoast.showToast(msg: 'Kết nối thất bại, vui lòng kiểm tra mạng');
      socket.connect();
    });
    socket.onReconnectError((_) {
      Fluttertoast.showToast(msg: 'Kết nối thất bại, vui lòng kiểm tra mạng');
      socket.connect();
    });
    socket.on('UNREAD_COUNT_CHANGED', (data) {
      final res = UnreadCountChanged.fromJson(data);
      listChatRoom.value = listChatRoom.value
          .map((e) => e.id == res.roomId
              ? RoomItem(
                  id: e.id,
                  name: e.name,
                  avatar: e.avatar,
                  unreadCount: res.count!,
                )
              : e)
          .toList();
      listGroupChatRoom.value = listGroupChatRoom.value
          .map((e) => e.id == res.roomId
              ? RoomItem(
                  id: e.id,
                  name: e.name,
                  avatar: e.avatar,
                  unreadCount: res.count!,
                )
              : e)
          .toList();
    });
    socket.on('ROOM_CREATED', (data) async {
      final res = RoomCreated.fromJson(data);
      await getUserInfo();
      if (userInfo.value.id == res.createdByUserId) {
        prevRoomId = activeRoomId.value;
        activeRoomId.value = res.roomId!;
      }
    });
    socket.on('RECEIVED_CHAT', (data) {
      final res = MessageResponse.fromJson(data);
      if (activeRoomId.value != res.roomId) return;
      messages.insert(
        0,
        res.type == 0
            ? types.TextMessage(
                author: types.User(
                    id: '${res.userId}', firstName: res.user!.fullName),
                id: '${res.id}',
                text: res.content!,
                createdAt: res.createdAt!.millisecondsSinceEpoch,
              )
            : res.type == 1
                ? types.ImageMessage(
                    author: types.User(
                        id: '${res.userId}', firstName: res.user!.fullName),
                    id: '${res.id}',
                    name: res.fileName!,
                    size: res.fileSize!,
                    uri: '${AppConstants.baseUrl}/${res.fileName!}',
                    createdAt: res.createdAt!.millisecondsSinceEpoch,
                  )
                : types.FileMessage(
                    author: types.User(
                        id: '${res.userId}', firstName: res.user!.fullName),
                    id: '${res.id}',
                    name: res.fileName!,
                    size: res.fileSize!,
                    uri: '${AppConstants.baseUrl}/${res.fileName!}',
                    createdAt: res.createdAt!.millisecondsSinceEpoch,
                  ),
      );
    });
    socket.on('PINNED_CHANGED', (data) {
      final response = MessageResponse.fromJson(data);
      if (response.roomId == activeRoomId.value) {
        final index = listMsgTmp.indexWhere((e) => e.id == response.id);
        listMsgTmp[index] =
            listMsgTmp[index].copyWith(isPinned: response.isPinned);
      }
    });
  }

  void subscribeUserInfoListener() {
    userInfo.listen((info) {
      listChatRoom.clear();
      listGroupChatRoom.clear();
      for (var e in info.rooms!) {
        if (e.name != null) {
          listGroupChatRoom.add(
            RoomItem(
              id: e.id!,
              name: e.name!,
              avatar: '',
              unreadCount: e.unreads!
                  .firstWhere((j) => j.userId == userInfo.value.id)
                  .count!,
            ),
          );
          continue;
        }
        final roomName = e.users!.firstWhere((k) => k.id != info.id).fullName;
        listChatRoom.add(
          RoomItem(
            id: e.id!,
            name: roomName!,
            avatar: roomName.split(' ').last[0],
            unreadCount: e.unreads!
                .firstWhere((j) => j.userId == userInfo.value.id)
                .count!,
          ),
        );
      }
      var findRoom = listChatRoom.value.firstWhereOrNull(
        (e) => e.id == activeRoomId.value,
      );
      findRoom ??= listGroupChatRoom.value.firstWhereOrNull(
        (e) => e.id == activeRoomId.value,
      );
      currentRoom.value = findRoom;
    });
  }

  void subscribeActiveRoomIdListener() {
    activeRoomId.listen((id) {
      joinRoom();
      currentPage.value = 1;
      getListMessage(currentPage.value);
      var findRoom = listChatRoom.value.firstWhereOrNull((e) => e.id == id);
      findRoom ??= listGroupChatRoom.value.firstWhereOrNull((e) => e.id == id);
      currentRoom.value = findRoom;
    });
  }

  void initializeListeners() {
    subscribeUserInfoListener();
    subscribeActiveRoomIdListener();
  }

  Future<void> getUserInfo() async {
    final res = await apiRepository.getUserInfo();
    if (res.statusCode != HttpStatus.ok) return;
    final response = UserInfoResponse.fromJson(res.body);
    userInfo.value = response.data!;
  }

  void logout() {
    box.remove(StorageKeys.accessToken);
    Get.offAllNamed(Routes.LOGIN);
  }

  Future<void> sendChatText(String text) async {
    final res = await apiRepository.sendChatText(ChatTextBody(
      room: activeRoomId.value,
      content: text,
    ));
    if (res.statusCode != HttpStatus.ok) return;
    final response = ChatTextResponse.fromJson(res.body);
    messages.insert(
        0,
        types.TextMessage(
          author: types.User(
              id: '${response.data!.userId}',
              firstName: response.data!.user!.fullName),
          id: '${response.data!.id}',
          text: response.data!.content!,
          createdAt: response.data!.createdAt!.millisecondsSinceEpoch,
        ));
  }

  Future<void> updatePinned(int id, bool pinned) async {
    final res = await apiRepository.updatePinned(id, pinned);
    if (res.statusCode != HttpStatus.ok) return;
    final response = ChatTextResponse.fromJson(res.body);
    final index = listMsgTmp.indexWhere((e) => e.id == response.data!.id);
    listMsgTmp[index] =
        listMsgTmp[index].copyWith(isPinned: response.data!.isPinned);
  }
}

class RoomItem {
  RoomItem({
    required this.id,
    required this.name,
    required this.avatar,
    this.unreadCount = 0,
  });

  final String avatar;
  final int id;
  final String name;
  final int unreadCount;
}
