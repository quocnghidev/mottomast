import 'package:get/get.dart';
import 'package:mottomast/modules/home/home_controller.dart';

class HomeBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(HomeController(apiRepository: Get.find()));
  }
}
