import 'package:flutter/material.dart';
import 'package:mottomast/theme/app_colors.dart';

class LoadingDialog extends StatelessWidget {
  const LoadingDialog({super.key});

  @override
  Widget build(BuildContext context) {
    return Dialog(
      clipBehavior: Clip.antiAlias,
      elevation: 0,
      child: Container(
        padding: const EdgeInsets.symmetric(vertical: 24, horizontal: 32),
        color: Colors.white,
        child: Row(
          children: const [
            SizedBox(
              width: 12,
              height: 12,
              child: CircularProgressIndicator(
                strokeWidth: 2,
              ),
            ),
            SizedBox(width: 20),
            Text(
              'Đang xử lý...',
              style: TextStyle(
                fontSize: 16,
                color: AppColors.secondaryText,
              ),
            )
          ],
        ),
      ),
    );
  }
}
