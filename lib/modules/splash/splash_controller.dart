import 'package:get/get.dart';
import 'package:mottomast/routes/routes.dart';

class SplashController extends GetxController {
  @override
  void onReady() {
    super.onReady();
    Future.delayed(
      const Duration(milliseconds: 1500),
      () => Get.offAllNamed(Routes.HOME),
    );
  }
}
