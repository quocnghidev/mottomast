// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/models/base_error/base_error.dart';
import 'package:mottomast/models/create_room_body/create_room_body.dart';
import 'package:mottomast/models/list_user_response/list_user_response.dart';
import 'package:mottomast/models/user_info_response/user.dart';

class CreateChannelChatController extends GetxController {
  CreateChannelChatController({required this.apiRepository});

  final ApiRepository apiRepository;
  RxString createError = ''.obs;
  var listTmpData = <User>[].obs;
  final listUser = <User>[].obs;
  final nameField = TextEditingController();
  final searchField = TextEditingController();
  final targets = <User>[].obs;

  @override
  void onClose() {
    nameField.dispose();
    searchField.dispose();
    super.onClose();
  }

  @override
  void onInit() {
    super.onInit();
    getAllUser();
  }

  Future<void> getAllUser() async {
    final res = await apiRepository.getAllUser(false);
    if (res.statusCode != HttpStatus.ok) {
      Get.back();
      return;
    }
    final response = ListUserResponse.fromJson(res.body);
    listTmpData.value = response.data!;
    search();
  }

  void search() {
    final query = searchField.text.trim();
    listUser.value = listTmpData
        .where(
          (e) =>
              e.fullName!.contains(RegExp(query, caseSensitive: false)) ||
              e.nickName!.contains(RegExp(query, caseSensitive: false)),
        )
        .toList();
    for (var user in targets) {
      listUser.removeWhere((e) => e.id == user.id);
    }
  }

  void createChannelChat() {
    createError.value = '';
    final nameChannel = nameField.text.trim();
    if (nameChannel.isEmpty) {
      createError.value = 'Vui lòng nhập đầy đủ tên kênh chat';
      return;
    }
    if (targets.value.length <= 1) {
      createError.value = 'Vui lòng chọn ít nhất 2 thành viên';
      return;
    }
    createRoom(nameChannel);
  }

  void addUser(User user) {
    targets.add(user);
    listUser.removeWhere((e) => e.id == user.id);
  }

  void removeUser(User user) {
    targets.removeWhere((e) => e.id == user.id);
    search();
  }

  Future<void> createRoom(String name) async {
    final res = await apiRepository.createRoom(
      CreateRoomBody(
        targets: targets.map((e) => e.id!).toList(),
        name: name,
      ),
    );
    if (res.statusCode == HttpStatus.ok) {
      Get.back();
    } else {
      final response = BaseError.fromJson(res.body);
      switch (response.error) {
        case 'Room Existed':
          Fluttertoast.showToast(
              msg:
                  'Đã tồn tại cuộc trò chuyện này, vui lòng chọn thành viên khác');
          break;
        default:
          Fluttertoast.showToast(msg: 'Có lỗi xảy ra, vui lòng thử lại sau');
          break;
      }
    }
  }
}
