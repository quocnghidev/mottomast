import 'package:get/get.dart';
import 'package:mottomast/modules/create_channel_chat/create_channel_chat_controller.dart';

class CreateChannelChatBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreateChannelChatController(apiRepository: Get.find()));
  }
}
