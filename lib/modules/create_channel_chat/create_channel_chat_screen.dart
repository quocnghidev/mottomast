// ignore_for_file: invalid_use_of_protected_member

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mottomast/modules/create_channel_chat/create_channel_chat_controller.dart';
import 'package:mottomast/theme/app_colors.dart';

class CreateChannelChatScreen extends GetView<CreateChannelChatController> {
  const CreateChannelChatScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.focusScope?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Tạo kênh chat mới'),
          centerTitle: true,
          actions: [
            IconButton(
              onPressed: () => controller.createChannelChat(),
              icon: const Icon(
                Icons.done_rounded,
                size: 28,
              ),
            ),
          ],
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 12, right: 12),
              child: Column(
                children: [
                  Obx(
                    () => controller.createError.value.isNotEmpty
                        ? SizedBox(
                            width: double.infinity,
                            child: Column(
                              children: [
                                Text(
                                  controller.createError.value,
                                  style: const TextStyle(
                                    fontSize: 12,
                                    color: AppColors.errorText,
                                  ),
                                ),
                                const SizedBox(height: 16),
                              ],
                            ),
                          )
                        : const SizedBox(),
                  ),
                  TextField(
                    controller: controller.nameField,
                    textInputAction: TextInputAction.done,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 10,
                      ),
                      border: OutlineInputBorder(),
                      hintText: 'Đặt tên kênh chat',
                    ),
                  ),
                  const SizedBox(height: 8),
                  TextField(
                    controller: controller.searchField,
                    onChanged: (_) => controller.search(),
                    textInputAction: TextInputAction.search,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 10,
                      ),
                      border: OutlineInputBorder(),
                      hintText: 'Tìm kiếm người dùng',
                    ),
                  ),
                  const SizedBox(height: 4),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Obx(
                        () => Text(
                          'Đã chọn ${controller.targets.value.length} người',
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            color: AppColors.secondaryText,
                          ),
                        ),
                      ),
                      Obx(
                        () => Text(
                          'Tổng: ${controller.listUser.value.length} trên ${controller.listTmpData.length} người',
                          style: const TextStyle(
                            fontSize: 13,
                            fontWeight: FontWeight.w500,
                            color: AppColors.secondaryText,
                          ),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 4),
                  SizedBox(
                    width: double.infinity,
                    child: Obx(
                      () => Wrap(
                        spacing: 8,
                        children: List.generate(
                          controller.targets.value.length,
                          (i) => InputChip(
                            onDeleted: () => controller.removeUser(
                              controller.targets.value[i],
                            ),
                            deleteIconColor: Colors.black54,
                            labelPadding:
                                const EdgeInsets.only(left: 6, right: 2),
                            label: Text(
                              controller.listTmpData.value
                                  .firstWhere((e) =>
                                      e.id == controller.targets.value[i].id)
                                  .nickName!,
                              style: const TextStyle(
                                fontSize: 13,
                                color: AppColors.primaryText,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () => controller.getAllUser(),
                child: Obx(
                  () => ListView.builder(
                    itemBuilder: (context, index) {
                      return Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              width: 1,
                              color: const Color(0xFF000000).withOpacity(0.15),
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 16,
                              backgroundColor: AppColors.primaryColor,
                              child: Text(
                                controller.listUser.value[index].fullName!
                                    .split(' ')
                                    .last[0],
                                style: const TextStyle(
                                  fontWeight: FontWeight.w500,
                                  height: 1,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "@${controller.listUser.value[index].nickName ?? ''}",
                                      style: const TextStyle(
                                        fontSize: 16,
                                        color: AppColors.primaryText,
                                      ),
                                    ),
                                    Text(
                                      " - ${controller.listUser.value[index].fullName ?? ''}",
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: AppColors.secondaryText,
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 6),
                                Text(
                                  controller.listUser.value[index].email ?? '',
                                  style: const TextStyle(
                                    fontSize: 14,
                                    color: AppColors.secondaryText,
                                  ),
                                ),
                              ],
                            ),
                            const Expanded(child: SizedBox()),
                            InkWell(
                              onTap: () => controller
                                  .addUser(controller.listUser.value[index]),
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                color: Colors.transparent,
                                child: const Icon(
                                  Icons.add,
                                  color: AppColors.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    itemCount: controller.listUser.value.length,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
