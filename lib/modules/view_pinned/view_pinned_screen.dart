// ignore_for_file: invalid_use_of_protected_member, depend_on_referenced_packages

import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart';
import 'package:get/get.dart';
import 'package:http/http.dart' as http;
import 'package:mottomast/modules/home/chat_l10n.dart';
import 'package:mottomast/modules/home/chat_theme.dart';
import 'package:mottomast/modules/home/models/emoji_enlargement_behavior.dart';
import 'package:mottomast/modules/home/models/send_button_visibility_mode.dart';
import 'package:mottomast/modules/home/widgets/chat.dart';
import 'package:mottomast/modules/home/widgets/input/input.dart';
import 'package:mottomast/modules/view_pinned/view_pinned_controller.dart';
import 'package:mottomast/theme/app_colors.dart';
import 'package:open_filex/open_filex.dart';
import 'package:path_provider/path_provider.dart';

class ViewPinnedScreen extends GetView<ViewPinnedController> {
  const ViewPinnedScreen({super.key});

  void _handlePreviewDataFetched(
    TextMessage message,
    PreviewData previewData,
  ) {
    final index = controller.messages.value
        .indexWhere((element) => element.id == message.id);
    final updatedMessage =
        (controller.messages.value[index] as TextMessage).copyWith(
      previewData: previewData,
    );

    controller.messages.value[index] = updatedMessage;
  }

  void _handleMessageTap(BuildContext _, Message message) async {
    if (message is FileMessage) {
      var localPath = message.uri;

      if (message.uri.startsWith('http')) {
        try {
          // Update tapped file message to show loading spinner
          final index = controller.messages.value
              .indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (controller.messages.value[index] as FileMessage).copyWith(
            isLoading: true,
          );

          controller.messages.value[index] = updatedMessage;

          final client = http.Client();
          final request = await client.get(Uri.parse(message.uri));
          final bytes = request.bodyBytes;
          final documentsDir = (await getApplicationDocumentsDirectory()).path;
          localPath = '$documentsDir/${message.name}';

          if (!File(localPath).existsSync()) {
            final file = File(localPath);
            await file.writeAsBytes(bytes);
          }
        } finally {
          // In case of error or success, reset loading spinner
          final index = controller.messages.value
              .indexWhere((element) => element.id == message.id);
          final updatedMessage =
              (controller.messages.value[index] as FileMessage).copyWith(
            isLoading: null,
          );
          controller.messages.value[index] = updatedMessage;
        }
      }

      await OpenFilex.open(localPath);
    }
  }

  @override
  Widget build(BuildContext context) {
    final homeController = controller.homeController;
    return Scaffold(
      appBar: AppBar(
        title: const Text('Tin nhắn đã ghim'),
        centerTitle: true,
      ),
      body: Obx(
        () => Chat(
          customBottomWidget: const SizedBox(),
          inputOptions: const InputOptions(
            sendButtonVisibilityMode: SendButtonVisibilityMode.always,
          ),
          emojiEnlargementBehavior: EmojiEnlargementBehavior.never,
          theme: const DefaultChatTheme(
            inputTextColor: AppColors.primaryText,
            inputBackgroundColor: Color(0xFFf2f3f5),
            inputMargin: EdgeInsets.all(12),
            inputPadding: EdgeInsets.symmetric(vertical: 12, horizontal: 6),
            inputBorderRadius: BorderRadius.all(Radius.circular(40)),
            primaryColor: AppColors.secondaryColor,
          ),
          showUserAvatars: true,
          showUserNames: true,
          dateLocale: 'vi_VN',
          l10n: const ChatL10nEn(
            inputPlaceholder: 'Nhắn tin',
            emptyChatPlaceholder: 'Không có tin nhắn nào hiển thị',
            sendButtonAccessibilityLabel: 'Gửi',
            unreadMessagesLabel: 'Tin nhắn chưa đọc',
            fileButtonAccessibilityLabel: 'Tệp',
            attachmentButtonAccessibilityLabel: 'Hình ảnh',
          ),
          messages: controller.messages.value,
          onMessageTap: _handleMessageTap,
          onPreviewDataFetched: _handlePreviewDataFetched,
          user: User(
            id: '${homeController.userInfo.value.id}',
            firstName: homeController.userInfo.value.fullName,
          ),
          onSendPressed: (msg) {},
        ),
      ),
    );
  }
}
