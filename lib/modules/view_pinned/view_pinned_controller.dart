// ignore_for_file: invalid_use_of_protected_member

import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/core/constants/app_constants.dart';
import 'package:mottomast/models/list_message_response/message_response.dart';
import 'package:flutter_chat_types/flutter_chat_types.dart' as types;
import 'package:mottomast/models/list_pinned_response/list_pinned_response.dart';
import 'package:mottomast/modules/home/home_controller.dart';

class ViewPinnedController extends GetxController {
  ViewPinnedController({
    required this.apiRepository,
    required this.homeController,
  });

  final ApiRepository apiRepository;
  final HomeController homeController;
  final listPinned = <MessageResponse>[].obs;
  final messages = <types.Message>[].obs;

  @override
  void onInit() {
    super.onInit();
    getListPinned();
  }

  Future<void> getListPinned() async {
    final res =
        await apiRepository.getListPinned(homeController.activeRoomId.value);
    if (res.statusCode != HttpStatus.ok) return;
    final response = ListPinnedResponse.fromJson(res.body);
    listPinned.value = response.data!;
    messages.value = listPinned.value.map((e) {
      if (e.type == 1) {
        return types.ImageMessage(
          author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
          id: '${e.id}',
          name: e.fileName!,
          size: e.fileSize!,
          uri: '${AppConstants.baseUrl}/${e.fileName!}',
          createdAt: e.createdAt!.millisecondsSinceEpoch,
        );
      }
      if (e.type == 2) {
        return types.FileMessage(
          author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
          id: '${e.id}',
          name: e.fileName!,
          size: e.fileSize!,
          uri: '${AppConstants.baseUrl}/${e.fileName!}',
          createdAt: e.createdAt!.millisecondsSinceEpoch,
        );
      }
      return types.TextMessage(
        author: types.User(id: '${e.userId}', firstName: e.user!.fullName),
        id: '${e.id}',
        text: e.content!,
        createdAt: e.createdAt!.millisecondsSinceEpoch,
      );
    }).toList();
  }
}
