import 'package:get/get.dart';
import 'package:mottomast/modules/view_pinned/view_pinned_controller.dart';

class ViewPinnedBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ViewPinnedController(
        apiRepository: Get.find(), homeController: Get.find()));
  }
}
