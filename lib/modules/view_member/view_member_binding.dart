import 'package:get/get.dart';
import 'package:mottomast/modules/view_member/view_member_controller.dart';

class ViewMemberBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => ViewMemberController(apiRepository: Get.find(), homeController: Get.find()));
  }
}
