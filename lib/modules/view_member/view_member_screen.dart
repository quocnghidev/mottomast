import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mottomast/modules/view_member/view_member_controller.dart';
import 'package:mottomast/theme/app_colors.dart';

class ViewMemberScreen extends GetView<ViewMemberController> {
  const ViewMemberScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Danh sách thành viên'),
        centerTitle: true,
      ),
      body: Obx(
        () => ListView.builder(
          itemBuilder: (context, index) {
            return Container(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 16),
              decoration: BoxDecoration(
                border: Border(
                  bottom: BorderSide(
                    width: 1,
                    color: const Color(0xFF000000).withOpacity(0.15),
                  ),
                ),
              ),
              child: Row(
                children: [
                  CircleAvatar(
                    radius: 16,
                    backgroundColor: AppColors.primaryColor,
                    child: Text(
                      controller.listUsers[index].fullName!.split(' ').last[0],
                      style: const TextStyle(
                        fontWeight: FontWeight.w500,
                        height: 1,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  const SizedBox(width: 16),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          Text(
                            "@${controller.listUsers[index].nickName ?? ''}",
                            style: const TextStyle(
                              fontSize: 16,
                              color: AppColors.primaryText,
                            ),
                          ),
                          Text(
                            " - ${controller.listUsers[index].fullName ?? ''}",
                            style: const TextStyle(
                              fontSize: 16,
                              fontWeight: FontWeight.w500,
                              color: AppColors.secondaryText,
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(height: 6),
                      Text(
                        controller.listUsers[index].email ?? '',
                        style: const TextStyle(
                          fontSize: 14,
                          color: AppColors.secondaryText,
                        ),
                      ),
                    ],
                  ),
                  const Expanded(child: SizedBox()),
                ],
              ),
            );
          },
          itemCount: controller.listUsers.length,
        ),
      ),
    );
  }
}
