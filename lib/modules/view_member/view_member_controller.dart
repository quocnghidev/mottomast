import 'package:get/get.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/models/user_info_response/user.dart';
import 'package:mottomast/modules/home/home_controller.dart';

class ViewMemberController extends GetxController {
  ViewMemberController({
    required this.apiRepository,
    required this.homeController,
  });

  final ApiRepository apiRepository;
  final HomeController homeController;
  final listUsers = <User>[].obs;

  @override
  void onInit() {
    super.onInit();
    getUsers();
  }

  void getUsers() {
    final users = homeController.userInfo.value.rooms!
        .firstWhere((e) => e.id == homeController.activeRoomId.value)
        .users!;
    listUsers.value = users;
  }
}
