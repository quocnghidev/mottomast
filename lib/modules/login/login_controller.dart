import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/core/constants/storage_keys.dart';
import 'package:mottomast/models/base_error/base_error.dart';
import 'package:mottomast/models/login_body/login_body.dart';
import 'package:mottomast/models/login_response/login_response.dart';
import 'package:mottomast/routes/routes.dart';

class LoginController extends GetxController {
  LoginController({required this.apiRepository});

  final ApiRepository apiRepository;
  final box = GetStorage();
  final emailField = TextEditingController();
  RxBool isLoading = false.obs;
  RxString loginError = ''.obs;
  final passwordField = TextEditingController();

  @override
  void onClose() {
    emailField.dispose();
    passwordField.dispose();
    super.onClose();
  }

  Future<void> login() async {
    final email = emailField.text.trim();
    final password = passwordField.text.trim();

    if (email.isEmpty || password.isEmpty) {
      loginError.value = 'Vui lòng nhập đầy đủ tất cả các trường';
      return;
    }

    isLoading.value = true;

    final res = await apiRepository.login(
      LoginBody(
        email: email,
        password: password,
      ),
    );

    if (res.statusCode == HttpStatus.ok) {
      final response = LoginResponse.fromJson(res.body);
      box.write(StorageKeys.accessToken, response.data!.accessToken!);
      Get.offAllNamed(Routes.HOME);
    } else {
      final response = BaseError.fromJson(res.body);
      switch (response.error) {
        case 'Wrong Credentials':
          loginError.value = 'Địa chỉ Email hoặc mật khẩu không chính xác';
          break;
        default:
          loginError.value = 'Có lỗi xảy ra, vui lòng thử lại sau';
          break;
      }
    }

    isLoading.value = false;
  }
}
