import 'package:flutter/cupertino.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:mottomast/api/api_repository.dart';
import 'package:mottomast/models/base_error/base_error.dart';
import 'package:mottomast/models/create_room_body/create_room_body.dart';
import 'package:mottomast/models/list_user_response/list_user_response.dart';
import 'package:mottomast/models/user_info_response/user.dart';

class CreateDirectChatController extends GetxController {
  CreateDirectChatController({required this.apiRepository});

  final ApiRepository apiRepository;
  var listTmpData = <User>[];
  final listUser = <User>[].obs;
  final searchField = TextEditingController();

  @override
  void onClose() {
    searchField.dispose();
    super.onClose();
  }

  @override
  void onInit() {
    super.onInit();
    getAllUser();
  }

  Future<void> createRoom(int id) async {
    final res = await apiRepository.createRoom(
      CreateRoomBody(
        targets: [id],
        name: '',
      ),
    );
    if (res.statusCode == HttpStatus.ok) {
      Get.back();
    } else {
      final response = BaseError.fromJson(res.body);
      switch (response.error) {
        case 'Room Existed':
          Fluttertoast.showToast(
              msg:
                  'Đã tồn tại cuộc trò chuyện này, vui lòng chọn thành viên khác');
          break;
        default:
          Fluttertoast.showToast(msg: 'Có lỗi xảy ra, vui lòng thử lại sau');
          break;
      }
    }
  }

  Future<void> getAllUser() async {
    final res = await apiRepository.getAllUser(false);
    if (res.statusCode != HttpStatus.ok) {
      Get.back();
      return;
    }
    final response = ListUserResponse.fromJson(res.body);
    listTmpData = response.data!;
    search();
  }

  void search() {
    final query = searchField.text.trim();
    listUser.value = listTmpData
        .where(
          (e) =>
              e.fullName!.contains(RegExp(query, caseSensitive: false)) ||
              e.nickName!.contains(RegExp(query, caseSensitive: false)),
        )
        .toList();
  }
}
