import 'package:get/get.dart';
import 'package:mottomast/modules/create_direct_chat/create_direct_chat_controller.dart';

class CreateDirectChatBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => CreateDirectChatController(apiRepository: Get.find()));
  }
}
