import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mottomast/modules/create_direct_chat/create_direct_chat_controller.dart';
import 'package:mottomast/theme/app_colors.dart';

class CreateDirectChatScreen extends GetView<CreateDirectChatController> {
  const CreateDirectChatScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => Get.focusScope?.unfocus(),
      child: Scaffold(
        appBar: AppBar(
          title: const Text('Tạo cuộc trò chuyện mới'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 10, left: 12, right: 12),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  TextField(
                    controller: controller.searchField,
                    onChanged: (_) => controller.search(),
                    textInputAction: TextInputAction.search,
                    decoration: const InputDecoration(
                      contentPadding: EdgeInsets.symmetric(
                        vertical: 10,
                        horizontal: 10,
                      ),
                      border: OutlineInputBorder(),
                      hintText: 'Tìm kiếm người dùng',
                    ),
                  ),
                  const SizedBox(height: 4),
                  Obx(
                    () => Text(
                      'Tổng: ${controller.listUser.length} trên ${controller.listTmpData.length} người',
                      style: const TextStyle(
                        fontSize: 13,
                        fontWeight: FontWeight.w500,
                        color: AppColors.secondaryText,
                      ),
                    ),
                  ),
                  const SizedBox(height: 4),
                ],
              ),
            ),
            Expanded(
              child: RefreshIndicator(
                onRefresh: () => controller.getAllUser(),
                child: Obx(
                  () => ListView.builder(
                    itemBuilder: (context, index) {
                      return Container(
                        padding: const EdgeInsets.symmetric(
                            horizontal: 16, vertical: 16),
                        decoration: BoxDecoration(
                          border: Border(
                            bottom: BorderSide(
                              width: 1,
                              color: const Color(0xFF000000).withOpacity(0.15),
                            ),
                          ),
                        ),
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 16,
                              backgroundColor: AppColors.primaryColor,
                              child: Text(
                                controller.listUser[index].fullName!
                                    .split(' ')
                                    .last[0],
                                style: const TextStyle(
                                  fontWeight: FontWeight.w500,
                                  height: 1,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                            const SizedBox(width: 16),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  children: [
                                    Text(
                                      "@${controller.listUser[index].nickName ?? ''}",
                                      style: const TextStyle(
                                        fontSize: 16,
                                        color: AppColors.primaryText,
                                      ),
                                    ),
                                    Text(
                                      " - ${controller.listUser[index].fullName ?? ''}",
                                      style: const TextStyle(
                                        fontSize: 16,
                                        fontWeight: FontWeight.w500,
                                        color: AppColors.secondaryText,
                                      ),
                                    ),
                                  ],
                                ),
                                const SizedBox(height: 6),
                                Text(
                                  controller.listUser[index].email ?? '',
                                  style: const TextStyle(
                                    fontSize: 14,
                                    color: AppColors.secondaryText,
                                  ),
                                ),
                              ],
                            ),
                            const Expanded(child: SizedBox()),
                            InkWell(
                              onTap: () => controller
                                  .createRoom(controller.listUser[index].id!),
                              customBorder: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                              ),
                              child: Container(
                                padding: const EdgeInsets.all(8),
                                color: Colors.transparent,
                                child: const Icon(
                                  Icons.add,
                                  color: AppColors.primaryColor,
                                ),
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                    itemCount: controller.listUser.length,
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
