import 'package:get/get.dart';
import 'package:mottomast/api/api_provider.dart';
import 'package:mottomast/models/chat_text_body/chat_text_body.dart';
import 'package:mottomast/models/create_room_body/create_room_body.dart';
import 'package:mottomast/models/login_body/login_body.dart';
import 'package:mottomast/models/register_body/register_body.dart';

class ApiRepository extends GetxService {
  ApiRepository({required this.apiProvider});

  final ApiProvider apiProvider;

  Future<Response> login(LoginBody body) async {
    return apiProvider.login('/auth/login', body);
  }

  Future<Response> register(RegisterBody body) async {
    return apiProvider.register('/auth/register', body);
  }

  Future<Response> getUserInfo() async {
    return apiProvider.getUserInfo('/user/info');
  }

  Future<Response> getAllUser(bool keep) async {
    return apiProvider.getAllUser('/user/get-all?keep=$keep');
  }

  Future<Response> createRoom(CreateRoomBody body) async {
    return apiProvider.createRoom('/user/create-room', body);
  }

  Future<Response> getListMessage(int roomId, int page) async {
    return apiProvider.getListMessage('/user/messages/$roomId?page=$page');
  }

  Future<Response> sendChatText(ChatTextBody body) async {
    return apiProvider.sendChatText('/user/chat', body);
  }

  Future<Response> sendChatFile(FormData body) async {
    return apiProvider.sendChatFile('/user/chat-file', body);
  }

  Future<Response> updatePinned(int id, bool pinned) async {
    return apiProvider
        .updatePinned('/user/message/update-pinned?id=$id&pinned=$pinned');
  }

  Future<Response> getListPinned(int id) async {
    return apiProvider.getListPinned('/user/messages/pinned/$id');
  }
}
