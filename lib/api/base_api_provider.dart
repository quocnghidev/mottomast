import 'dart:async';
import 'package:get/get.dart';
import 'package:get/get_connect/http/src/request/request.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mottomast/core/constants/app_constants.dart';
import 'package:mottomast/core/constants/storage_keys.dart';
import 'package:mottomast/routes/routes.dart';

class BaseApiProvider extends GetConnect {
  final box = GetStorage();

  @override
  void onInit() {
    httpClient.baseUrl = AppConstants.baseApiUrl;
    httpClient.timeout = const Duration(seconds: 30);
    httpClient.addRequestModifier(requestInterceptor);
    httpClient.addResponseModifier(responseInterceptor);
  }

  FutureOr<Request> requestInterceptor(request) async {
    String? token = box.read(StorageKeys.accessToken);
    if (token != null) request.headers['Authorization'] = 'Bearer $token';
    return request;
  }

  FutureOr<dynamic> responseInterceptor(
    Request request,
    Response response,
  ) async {
    if (response.hasError) {
      switch (response.statusCode) {
        case HttpStatus.unauthorized:
          logout();
          break;
        default:
          break;
      }
    }
    return response;
  }

  void logout() {
    box.remove(StorageKeys.accessToken);
    Get.offAllNamed(Routes.LOGIN);
  }
}
