import 'package:get/get.dart';
import 'package:mottomast/api/base_api_provider.dart';
import 'package:mottomast/models/chat_text_body/chat_text_body.dart';
import 'package:mottomast/models/create_room_body/create_room_body.dart';
import 'package:mottomast/models/login_body/login_body.dart';
import 'package:mottomast/models/register_body/register_body.dart';

class ApiProvider extends BaseApiProvider {
  Future<Response> login(String path, LoginBody body) {
    return post(path, body.toJson());
  }

  Future<Response> register(String path, RegisterBody body) {
    return post(path, body.toJson());
  }

  Future<Response> getUserInfo(String path) {
    return get(path);
  }

  Future<Response> getAllUser(String path) {
    return get(path);
  }

  Future<Response> createRoom(String path, CreateRoomBody body) {
    return post(path, body.toJson());
  }

  Future<Response> getListMessage(String path) {
    return get(path);
  }

  Future<Response> sendChatText(String path, ChatTextBody body) {
    return post(path, body.toJson());
  }

  Future<Response> sendChatFile(String path, FormData body) {
    return post(
      path,
      body,
      contentType: 'multipart/form-data',
    );
  }

  Future<Response> updatePinned(String path) {
    return post(path, null);
  }

  Future<Response> getListPinned(String path) {
    return get(path);
  }
}
