import 'package:freezed_annotation/freezed_annotation.dart';

import 'room.dart';

part 'data.freezed.dart';
part 'data.g.dart';

@freezed
class Data with _$Data {
  factory Data({
    int? id,
    String? email,
    String? fullName,
    String? nickName,
    bool? isDeleted,
    DateTime? createdAt,
    DateTime? updatedAt,
    List<Room>? rooms,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}
