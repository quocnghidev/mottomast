import 'package:freezed_annotation/freezed_annotation.dart';

part 'unread.freezed.dart';
part 'unread.g.dart';

@freezed
class Unread with _$Unread {
  factory Unread({
    int? id,
    int? userId,
    int? roomId,
    int? count,
    bool? isDeleted,
    DateTime? createdAt,
    DateTime? updatedAt,
  }) = _Unread;

  factory Unread.fromJson(Map<String, dynamic> json) => _$UnreadFromJson(json);
}
