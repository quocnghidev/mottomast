// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unread.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Unread _$$_UnreadFromJson(Map<String, dynamic> json) => _$_Unread(
      id: json['id'] as int?,
      userId: json['userId'] as int?,
      roomId: json['roomId'] as int?,
      count: json['count'] as int?,
      isDeleted: json['isDeleted'] as bool?,
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
    );

Map<String, dynamic> _$$_UnreadToJson(_$_Unread instance) => <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'roomId': instance.roomId,
      'count': instance.count,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
    };
