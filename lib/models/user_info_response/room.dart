import 'package:freezed_annotation/freezed_annotation.dart';

import 'unread.dart';
import 'user.dart';

part 'room.freezed.dart';
part 'room.g.dart';

@freezed
class Room with _$Room {
  factory Room({
    int? id,
    String? name,
    bool? isDeleted,
    DateTime? createdAt,
    DateTime? updatedAt,
    List<User>? users,
    List<Unread>? unreads,
  }) = _Room;

  factory Room.fromJson(Map<String, dynamic> json) => _$RoomFromJson(json);
}
