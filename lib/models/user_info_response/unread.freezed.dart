// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'unread.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

Unread _$UnreadFromJson(Map<String, dynamic> json) {
  return _Unread.fromJson(json);
}

/// @nodoc
mixin _$Unread {
  int? get id => throw _privateConstructorUsedError;
  int? get userId => throw _privateConstructorUsedError;
  int? get roomId => throw _privateConstructorUsedError;
  int? get count => throw _privateConstructorUsedError;
  bool? get isDeleted => throw _privateConstructorUsedError;
  DateTime? get createdAt => throw _privateConstructorUsedError;
  DateTime? get updatedAt => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UnreadCopyWith<Unread> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnreadCopyWith<$Res> {
  factory $UnreadCopyWith(Unread value, $Res Function(Unread) then) =
      _$UnreadCopyWithImpl<$Res, Unread>;
  @useResult
  $Res call(
      {int? id,
      int? userId,
      int? roomId,
      int? count,
      bool? isDeleted,
      DateTime? createdAt,
      DateTime? updatedAt});
}

/// @nodoc
class _$UnreadCopyWithImpl<$Res, $Val extends Unread>
    implements $UnreadCopyWith<$Res> {
  _$UnreadCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? userId = freezed,
    Object? roomId = freezed,
    Object? count = freezed,
    Object? isDeleted = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_value.copyWith(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      userId: freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
      roomId: freezed == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as int?,
      count: freezed == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int?,
      isDeleted: freezed == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UnreadCopyWith<$Res> implements $UnreadCopyWith<$Res> {
  factory _$$_UnreadCopyWith(_$_Unread value, $Res Function(_$_Unread) then) =
      __$$_UnreadCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call(
      {int? id,
      int? userId,
      int? roomId,
      int? count,
      bool? isDeleted,
      DateTime? createdAt,
      DateTime? updatedAt});
}

/// @nodoc
class __$$_UnreadCopyWithImpl<$Res>
    extends _$UnreadCopyWithImpl<$Res, _$_Unread>
    implements _$$_UnreadCopyWith<$Res> {
  __$$_UnreadCopyWithImpl(_$_Unread _value, $Res Function(_$_Unread) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? id = freezed,
    Object? userId = freezed,
    Object? roomId = freezed,
    Object? count = freezed,
    Object? isDeleted = freezed,
    Object? createdAt = freezed,
    Object? updatedAt = freezed,
  }) {
    return _then(_$_Unread(
      id: freezed == id
          ? _value.id
          : id // ignore: cast_nullable_to_non_nullable
              as int?,
      userId: freezed == userId
          ? _value.userId
          : userId // ignore: cast_nullable_to_non_nullable
              as int?,
      roomId: freezed == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as int?,
      count: freezed == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int?,
      isDeleted: freezed == isDeleted
          ? _value.isDeleted
          : isDeleted // ignore: cast_nullable_to_non_nullable
              as bool?,
      createdAt: freezed == createdAt
          ? _value.createdAt
          : createdAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
      updatedAt: freezed == updatedAt
          ? _value.updatedAt
          : updatedAt // ignore: cast_nullable_to_non_nullable
              as DateTime?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_Unread implements _Unread {
  _$_Unread(
      {this.id,
      this.userId,
      this.roomId,
      this.count,
      this.isDeleted,
      this.createdAt,
      this.updatedAt});

  factory _$_Unread.fromJson(Map<String, dynamic> json) =>
      _$$_UnreadFromJson(json);

  @override
  final int? id;
  @override
  final int? userId;
  @override
  final int? roomId;
  @override
  final int? count;
  @override
  final bool? isDeleted;
  @override
  final DateTime? createdAt;
  @override
  final DateTime? updatedAt;

  @override
  String toString() {
    return 'Unread(id: $id, userId: $userId, roomId: $roomId, count: $count, isDeleted: $isDeleted, createdAt: $createdAt, updatedAt: $updatedAt)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_Unread &&
            (identical(other.id, id) || other.id == id) &&
            (identical(other.userId, userId) || other.userId == userId) &&
            (identical(other.roomId, roomId) || other.roomId == roomId) &&
            (identical(other.count, count) || other.count == count) &&
            (identical(other.isDeleted, isDeleted) ||
                other.isDeleted == isDeleted) &&
            (identical(other.createdAt, createdAt) ||
                other.createdAt == createdAt) &&
            (identical(other.updatedAt, updatedAt) ||
                other.updatedAt == updatedAt));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, id, userId, roomId, count, isDeleted, createdAt, updatedAt);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UnreadCopyWith<_$_Unread> get copyWith =>
      __$$_UnreadCopyWithImpl<_$_Unread>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UnreadToJson(
      this,
    );
  }
}

abstract class _Unread implements Unread {
  factory _Unread(
      {final int? id,
      final int? userId,
      final int? roomId,
      final int? count,
      final bool? isDeleted,
      final DateTime? createdAt,
      final DateTime? updatedAt}) = _$_Unread;

  factory _Unread.fromJson(Map<String, dynamic> json) = _$_Unread.fromJson;

  @override
  int? get id;
  @override
  int? get userId;
  @override
  int? get roomId;
  @override
  int? get count;
  @override
  bool? get isDeleted;
  @override
  DateTime? get createdAt;
  @override
  DateTime? get updatedAt;
  @override
  @JsonKey(ignore: true)
  _$$_UnreadCopyWith<_$_Unread> get copyWith =>
      throw _privateConstructorUsedError;
}
