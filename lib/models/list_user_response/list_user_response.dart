import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mottomast/models/user_info_response/user.dart';

part 'list_user_response.freezed.dart';
part 'list_user_response.g.dart';

@freezed
class ListUserResponse with _$ListUserResponse {
  factory ListUserResponse({
    String? status,
    List<User>? data,
  }) = _ListUserResponse;

  factory ListUserResponse.fromJson(Map<String, dynamic> json) =>
      _$ListUserResponseFromJson(json);
}
