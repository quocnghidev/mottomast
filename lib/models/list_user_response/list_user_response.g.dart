// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_user_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ListUserResponse _$$_ListUserResponseFromJson(Map<String, dynamic> json) =>
    _$_ListUserResponse(
      status: json['status'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => User.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ListUserResponseToJson(_$_ListUserResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };
