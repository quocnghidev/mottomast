// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'list_user_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ListUserResponse _$ListUserResponseFromJson(Map<String, dynamic> json) {
  return _ListUserResponse.fromJson(json);
}

/// @nodoc
mixin _$ListUserResponse {
  String? get status => throw _privateConstructorUsedError;
  List<User>? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListUserResponseCopyWith<ListUserResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListUserResponseCopyWith<$Res> {
  factory $ListUserResponseCopyWith(
          ListUserResponse value, $Res Function(ListUserResponse) then) =
      _$ListUserResponseCopyWithImpl<$Res, ListUserResponse>;
  @useResult
  $Res call({String? status, List<User>? data});
}

/// @nodoc
class _$ListUserResponseCopyWithImpl<$Res, $Val extends ListUserResponse>
    implements $ListUserResponseCopyWith<$Res> {
  _$ListUserResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<User>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ListUserResponseCopyWith<$Res>
    implements $ListUserResponseCopyWith<$Res> {
  factory _$$_ListUserResponseCopyWith(
          _$_ListUserResponse value, $Res Function(_$_ListUserResponse) then) =
      __$$_ListUserResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? status, List<User>? data});
}

/// @nodoc
class __$$_ListUserResponseCopyWithImpl<$Res>
    extends _$ListUserResponseCopyWithImpl<$Res, _$_ListUserResponse>
    implements _$$_ListUserResponseCopyWith<$Res> {
  __$$_ListUserResponseCopyWithImpl(
      _$_ListUserResponse _value, $Res Function(_$_ListUserResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_ListUserResponse(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<User>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListUserResponse implements _ListUserResponse {
  _$_ListUserResponse({this.status, final List<User>? data}) : _data = data;

  factory _$_ListUserResponse.fromJson(Map<String, dynamic> json) =>
      _$$_ListUserResponseFromJson(json);

  @override
  final String? status;
  final List<User>? _data;
  @override
  List<User>? get data {
    final value = _data;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'ListUserResponse(status: $status, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ListUserResponse &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, status, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ListUserResponseCopyWith<_$_ListUserResponse> get copyWith =>
      __$$_ListUserResponseCopyWithImpl<_$_ListUserResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListUserResponseToJson(
      this,
    );
  }
}

abstract class _ListUserResponse implements ListUserResponse {
  factory _ListUserResponse({final String? status, final List<User>? data}) =
      _$_ListUserResponse;

  factory _ListUserResponse.fromJson(Map<String, dynamic> json) =
      _$_ListUserResponse.fromJson;

  @override
  String? get status;
  @override
  List<User>? get data;
  @override
  @JsonKey(ignore: true)
  _$$_ListUserResponseCopyWith<_$_ListUserResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
