// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'message_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_MessageResponse _$$_MessageResponseFromJson(Map<String, dynamic> json) =>
    _$_MessageResponse(
      id: json['id'] as int?,
      type: json['type'] as int?,
      content: json['content'] as String?,
      fileName: json['fileName'] as String?,
      fileSize: json['fileSize'] as num?,
      userId: json['userId'] as int?,
      isPinned: json['isPinned'] as bool?,
      roomId: json['roomId'] as int?,
      isDeleted: json['isDeleted'] as bool?,
      createdAt: json['createdAt'] == null
          ? null
          : DateTime.parse(json['createdAt'] as String),
      updatedAt: json['updatedAt'] == null
          ? null
          : DateTime.parse(json['updatedAt'] as String),
      user: json['user'] == null
          ? null
          : User.fromJson(json['user'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_MessageResponseToJson(_$_MessageResponse instance) =>
    <String, dynamic>{
      'id': instance.id,
      'type': instance.type,
      'content': instance.content,
      'fileName': instance.fileName,
      'fileSize': instance.fileSize,
      'userId': instance.userId,
      'isPinned': instance.isPinned,
      'roomId': instance.roomId,
      'isDeleted': instance.isDeleted,
      'createdAt': instance.createdAt?.toIso8601String(),
      'updatedAt': instance.updatedAt?.toIso8601String(),
      'user': instance.user,
    };
