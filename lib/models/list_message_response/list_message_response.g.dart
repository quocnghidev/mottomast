// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_message_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ListMessageResponse _$$_ListMessageResponseFromJson(
        Map<String, dynamic> json) =>
    _$_ListMessageResponse(
      status: json['status'] as String?,
      data: json['data'] == null
          ? null
          : Data.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ListMessageResponseToJson(
        _$_ListMessageResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };
