import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mottomast/models/user_info_response/user.dart';

part 'message_response.freezed.dart';
part 'message_response.g.dart';

@freezed
class MessageResponse with _$MessageResponse {
  factory MessageResponse({
    int? id,
    int? type,
    String? content,
    String? fileName,
    num? fileSize,
    int? userId,
    bool? isPinned,
    int? roomId,
    bool? isDeleted,
    DateTime? createdAt,
    DateTime? updatedAt,
    User? user,
  }) = _MessageResponse;

  factory MessageResponse.fromJson(Map<String, dynamic> json) =>
      _$MessageResponseFromJson(json);
}
