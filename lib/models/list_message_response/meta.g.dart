// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'meta.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_Meta _$$_MetaFromJson(Map<String, dynamic> json) => _$_Meta(
      total: json['total'] as int?,
      lastPage: json['lastPage'] as int?,
      currentPage: json['currentPage'] as int?,
      perPage: json['perPage'] as int?,
      prev: json['prev'] as int?,
      next: json['next'] as int?,
    );

Map<String, dynamic> _$$_MetaToJson(_$_Meta instance) => <String, dynamic>{
      'total': instance.total,
      'lastPage': instance.lastPage,
      'currentPage': instance.currentPage,
      'perPage': instance.perPage,
      'prev': instance.prev,
      'next': instance.next,
    };
