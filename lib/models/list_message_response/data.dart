import 'package:freezed_annotation/freezed_annotation.dart';

import 'message_response.dart';
import 'meta.dart';

part 'data.freezed.dart';
part 'data.g.dart';

@freezed
class Data with _$Data {
  factory Data({
    List<MessageResponse>? data,
    Meta? meta,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}
