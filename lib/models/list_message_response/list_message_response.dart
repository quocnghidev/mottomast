import 'package:freezed_annotation/freezed_annotation.dart';

import 'data.dart';

part 'list_message_response.freezed.dart';
part 'list_message_response.g.dart';

@freezed
class ListMessageResponse with _$ListMessageResponse {
  factory ListMessageResponse({
    String? status,
    Data? data,
  }) = _ListMessageResponse;

  factory ListMessageResponse.fromJson(Map<String, dynamic> json) =>
      _$ListMessageResponseFromJson(json);
}
