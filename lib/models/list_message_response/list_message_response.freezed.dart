// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'list_message_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ListMessageResponse _$ListMessageResponseFromJson(Map<String, dynamic> json) {
  return _ListMessageResponse.fromJson(json);
}

/// @nodoc
mixin _$ListMessageResponse {
  String? get status => throw _privateConstructorUsedError;
  Data? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListMessageResponseCopyWith<ListMessageResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListMessageResponseCopyWith<$Res> {
  factory $ListMessageResponseCopyWith(
          ListMessageResponse value, $Res Function(ListMessageResponse) then) =
      _$ListMessageResponseCopyWithImpl<$Res, ListMessageResponse>;
  @useResult
  $Res call({String? status, Data? data});

  $DataCopyWith<$Res>? get data;
}

/// @nodoc
class _$ListMessageResponseCopyWithImpl<$Res, $Val extends ListMessageResponse>
    implements $ListMessageResponseCopyWith<$Res> {
  _$ListMessageResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as Data?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $DataCopyWith<$Res>? get data {
    if (_value.data == null) {
      return null;
    }

    return $DataCopyWith<$Res>(_value.data!, (value) {
      return _then(_value.copyWith(data: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_ListMessageResponseCopyWith<$Res>
    implements $ListMessageResponseCopyWith<$Res> {
  factory _$$_ListMessageResponseCopyWith(_$_ListMessageResponse value,
          $Res Function(_$_ListMessageResponse) then) =
      __$$_ListMessageResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? status, Data? data});

  @override
  $DataCopyWith<$Res>? get data;
}

/// @nodoc
class __$$_ListMessageResponseCopyWithImpl<$Res>
    extends _$ListMessageResponseCopyWithImpl<$Res, _$_ListMessageResponse>
    implements _$$_ListMessageResponseCopyWith<$Res> {
  __$$_ListMessageResponseCopyWithImpl(_$_ListMessageResponse _value,
      $Res Function(_$_ListMessageResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_ListMessageResponse(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as Data?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListMessageResponse implements _ListMessageResponse {
  _$_ListMessageResponse({this.status, this.data});

  factory _$_ListMessageResponse.fromJson(Map<String, dynamic> json) =>
      _$$_ListMessageResponseFromJson(json);

  @override
  final String? status;
  @override
  final Data? data;

  @override
  String toString() {
    return 'ListMessageResponse(status: $status, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ListMessageResponse &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.data, data) || other.data == data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, status, data);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ListMessageResponseCopyWith<_$_ListMessageResponse> get copyWith =>
      __$$_ListMessageResponseCopyWithImpl<_$_ListMessageResponse>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListMessageResponseToJson(
      this,
    );
  }
}

abstract class _ListMessageResponse implements ListMessageResponse {
  factory _ListMessageResponse({final String? status, final Data? data}) =
      _$_ListMessageResponse;

  factory _ListMessageResponse.fromJson(Map<String, dynamic> json) =
      _$_ListMessageResponse.fromJson;

  @override
  String? get status;
  @override
  Data? get data;
  @override
  @JsonKey(ignore: true)
  _$$_ListMessageResponseCopyWith<_$_ListMessageResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
