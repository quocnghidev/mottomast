import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mottomast/models/user_info_response/room.dart';

part 'create_room_response.freezed.dart';
part 'create_room_response.g.dart';

@freezed
class CreateRoomResponse with _$CreateRoomResponse {
  factory CreateRoomResponse({
    String? status,
    Room? data,
  }) = _CreateRoomResponse;

  factory CreateRoomResponse.fromJson(Map<String, dynamic> json) =>
      _$CreateRoomResponseFromJson(json);
}
