// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_room_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreateRoomResponse _$$_CreateRoomResponseFromJson(
        Map<String, dynamic> json) =>
    _$_CreateRoomResponse(
      status: json['status'] as String?,
      data: json['data'] == null
          ? null
          : Room.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_CreateRoomResponseToJson(
        _$_CreateRoomResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };
