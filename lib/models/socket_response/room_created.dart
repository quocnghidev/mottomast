import 'package:freezed_annotation/freezed_annotation.dart';

part 'room_created.freezed.dart';
part 'room_created.g.dart';

@freezed
class RoomCreated with _$RoomCreated {
  factory RoomCreated({
    int? roomId,
    int? createdByUserId,
  }) = _RoomCreated;

  factory RoomCreated.fromJson(Map<String, dynamic> json) =>
      _$RoomCreatedFromJson(json);
}
