// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'room_created.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_RoomCreated _$$_RoomCreatedFromJson(Map<String, dynamic> json) =>
    _$_RoomCreated(
      roomId: json['roomId'] as int?,
      createdByUserId: json['createdByUserId'] as int?,
    );

Map<String, dynamic> _$$_RoomCreatedToJson(_$_RoomCreated instance) =>
    <String, dynamic>{
      'roomId': instance.roomId,
      'createdByUserId': instance.createdByUserId,
    };
