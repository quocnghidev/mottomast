// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'room_created.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

RoomCreated _$RoomCreatedFromJson(Map<String, dynamic> json) {
  return _RoomCreated.fromJson(json);
}

/// @nodoc
mixin _$RoomCreated {
  int? get roomId => throw _privateConstructorUsedError;
  int? get createdByUserId => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $RoomCreatedCopyWith<RoomCreated> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RoomCreatedCopyWith<$Res> {
  factory $RoomCreatedCopyWith(
          RoomCreated value, $Res Function(RoomCreated) then) =
      _$RoomCreatedCopyWithImpl<$Res, RoomCreated>;
  @useResult
  $Res call({int? roomId, int? createdByUserId});
}

/// @nodoc
class _$RoomCreatedCopyWithImpl<$Res, $Val extends RoomCreated>
    implements $RoomCreatedCopyWith<$Res> {
  _$RoomCreatedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? roomId = freezed,
    Object? createdByUserId = freezed,
  }) {
    return _then(_value.copyWith(
      roomId: freezed == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as int?,
      createdByUserId: freezed == createdByUserId
          ? _value.createdByUserId
          : createdByUserId // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_RoomCreatedCopyWith<$Res>
    implements $RoomCreatedCopyWith<$Res> {
  factory _$$_RoomCreatedCopyWith(
          _$_RoomCreated value, $Res Function(_$_RoomCreated) then) =
      __$$_RoomCreatedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? roomId, int? createdByUserId});
}

/// @nodoc
class __$$_RoomCreatedCopyWithImpl<$Res>
    extends _$RoomCreatedCopyWithImpl<$Res, _$_RoomCreated>
    implements _$$_RoomCreatedCopyWith<$Res> {
  __$$_RoomCreatedCopyWithImpl(
      _$_RoomCreated _value, $Res Function(_$_RoomCreated) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? roomId = freezed,
    Object? createdByUserId = freezed,
  }) {
    return _then(_$_RoomCreated(
      roomId: freezed == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as int?,
      createdByUserId: freezed == createdByUserId
          ? _value.createdByUserId
          : createdByUserId // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_RoomCreated implements _RoomCreated {
  _$_RoomCreated({this.roomId, this.createdByUserId});

  factory _$_RoomCreated.fromJson(Map<String, dynamic> json) =>
      _$$_RoomCreatedFromJson(json);

  @override
  final int? roomId;
  @override
  final int? createdByUserId;

  @override
  String toString() {
    return 'RoomCreated(roomId: $roomId, createdByUserId: $createdByUserId)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_RoomCreated &&
            (identical(other.roomId, roomId) || other.roomId == roomId) &&
            (identical(other.createdByUserId, createdByUserId) ||
                other.createdByUserId == createdByUserId));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, roomId, createdByUserId);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_RoomCreatedCopyWith<_$_RoomCreated> get copyWith =>
      __$$_RoomCreatedCopyWithImpl<_$_RoomCreated>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_RoomCreatedToJson(
      this,
    );
  }
}

abstract class _RoomCreated implements RoomCreated {
  factory _RoomCreated({final int? roomId, final int? createdByUserId}) =
      _$_RoomCreated;

  factory _RoomCreated.fromJson(Map<String, dynamic> json) =
      _$_RoomCreated.fromJson;

  @override
  int? get roomId;
  @override
  int? get createdByUserId;
  @override
  @JsonKey(ignore: true)
  _$$_RoomCreatedCopyWith<_$_RoomCreated> get copyWith =>
      throw _privateConstructorUsedError;
}
