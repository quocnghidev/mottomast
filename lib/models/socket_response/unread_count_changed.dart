import 'package:freezed_annotation/freezed_annotation.dart';

part 'unread_count_changed.freezed.dart';
part 'unread_count_changed.g.dart';

@freezed
class UnreadCountChanged with _$UnreadCountChanged {
  factory UnreadCountChanged({
    int? roomId,
    int? count,
  }) = _UnreadCountChanged;

  factory UnreadCountChanged.fromJson(Map<String, dynamic> json) =>
      _$UnreadCountChangedFromJson(json);
}
