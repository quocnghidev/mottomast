// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'unread_count_changed.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

UnreadCountChanged _$UnreadCountChangedFromJson(Map<String, dynamic> json) {
  return _UnreadCountChanged.fromJson(json);
}

/// @nodoc
mixin _$UnreadCountChanged {
  int? get roomId => throw _privateConstructorUsedError;
  int? get count => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $UnreadCountChangedCopyWith<UnreadCountChanged> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $UnreadCountChangedCopyWith<$Res> {
  factory $UnreadCountChangedCopyWith(
          UnreadCountChanged value, $Res Function(UnreadCountChanged) then) =
      _$UnreadCountChangedCopyWithImpl<$Res, UnreadCountChanged>;
  @useResult
  $Res call({int? roomId, int? count});
}

/// @nodoc
class _$UnreadCountChangedCopyWithImpl<$Res, $Val extends UnreadCountChanged>
    implements $UnreadCountChangedCopyWith<$Res> {
  _$UnreadCountChangedCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? roomId = freezed,
    Object? count = freezed,
  }) {
    return _then(_value.copyWith(
      roomId: freezed == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as int?,
      count: freezed == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_UnreadCountChangedCopyWith<$Res>
    implements $UnreadCountChangedCopyWith<$Res> {
  factory _$$_UnreadCountChangedCopyWith(_$_UnreadCountChanged value,
          $Res Function(_$_UnreadCountChanged) then) =
      __$$_UnreadCountChangedCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? roomId, int? count});
}

/// @nodoc
class __$$_UnreadCountChangedCopyWithImpl<$Res>
    extends _$UnreadCountChangedCopyWithImpl<$Res, _$_UnreadCountChanged>
    implements _$$_UnreadCountChangedCopyWith<$Res> {
  __$$_UnreadCountChangedCopyWithImpl(
      _$_UnreadCountChanged _value, $Res Function(_$_UnreadCountChanged) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? roomId = freezed,
    Object? count = freezed,
  }) {
    return _then(_$_UnreadCountChanged(
      roomId: freezed == roomId
          ? _value.roomId
          : roomId // ignore: cast_nullable_to_non_nullable
              as int?,
      count: freezed == count
          ? _value.count
          : count // ignore: cast_nullable_to_non_nullable
              as int?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_UnreadCountChanged implements _UnreadCountChanged {
  _$_UnreadCountChanged({this.roomId, this.count});

  factory _$_UnreadCountChanged.fromJson(Map<String, dynamic> json) =>
      _$$_UnreadCountChangedFromJson(json);

  @override
  final int? roomId;
  @override
  final int? count;

  @override
  String toString() {
    return 'UnreadCountChanged(roomId: $roomId, count: $count)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_UnreadCountChanged &&
            (identical(other.roomId, roomId) || other.roomId == roomId) &&
            (identical(other.count, count) || other.count == count));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, roomId, count);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_UnreadCountChangedCopyWith<_$_UnreadCountChanged> get copyWith =>
      __$$_UnreadCountChangedCopyWithImpl<_$_UnreadCountChanged>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_UnreadCountChangedToJson(
      this,
    );
  }
}

abstract class _UnreadCountChanged implements UnreadCountChanged {
  factory _UnreadCountChanged({final int? roomId, final int? count}) =
      _$_UnreadCountChanged;

  factory _UnreadCountChanged.fromJson(Map<String, dynamic> json) =
      _$_UnreadCountChanged.fromJson;

  @override
  int? get roomId;
  @override
  int? get count;
  @override
  @JsonKey(ignore: true)
  _$$_UnreadCountChangedCopyWith<_$_UnreadCountChanged> get copyWith =>
      throw _privateConstructorUsedError;
}
