// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'unread_count_changed.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_UnreadCountChanged _$$_UnreadCountChangedFromJson(
        Map<String, dynamic> json) =>
    _$_UnreadCountChanged(
      roomId: json['roomId'] as int?,
      count: json['count'] as int?,
    );

Map<String, dynamic> _$$_UnreadCountChangedToJson(
        _$_UnreadCountChanged instance) =>
    <String, dynamic>{
      'roomId': instance.roomId,
      'count': instance.count,
    };
