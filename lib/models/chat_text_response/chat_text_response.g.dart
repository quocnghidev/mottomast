// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_text_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatTextResponse _$$_ChatTextResponseFromJson(Map<String, dynamic> json) =>
    _$_ChatTextResponse(
      status: json['status'] as String?,
      data: json['data'] == null
          ? null
          : MessageResponse.fromJson(json['data'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$$_ChatTextResponseToJson(_$_ChatTextResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };
