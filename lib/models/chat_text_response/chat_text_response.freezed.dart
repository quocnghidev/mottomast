// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chat_text_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatTextResponse _$ChatTextResponseFromJson(Map<String, dynamic> json) {
  return _ChatTextResponse.fromJson(json);
}

/// @nodoc
mixin _$ChatTextResponse {
  String? get status => throw _privateConstructorUsedError;
  MessageResponse? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatTextResponseCopyWith<ChatTextResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatTextResponseCopyWith<$Res> {
  factory $ChatTextResponseCopyWith(
          ChatTextResponse value, $Res Function(ChatTextResponse) then) =
      _$ChatTextResponseCopyWithImpl<$Res, ChatTextResponse>;
  @useResult
  $Res call({String? status, MessageResponse? data});

  $MessageResponseCopyWith<$Res>? get data;
}

/// @nodoc
class _$ChatTextResponseCopyWithImpl<$Res, $Val extends ChatTextResponse>
    implements $ChatTextResponseCopyWith<$Res> {
  _$ChatTextResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as MessageResponse?,
    ) as $Val);
  }

  @override
  @pragma('vm:prefer-inline')
  $MessageResponseCopyWith<$Res>? get data {
    if (_value.data == null) {
      return null;
    }

    return $MessageResponseCopyWith<$Res>(_value.data!, (value) {
      return _then(_value.copyWith(data: value) as $Val);
    });
  }
}

/// @nodoc
abstract class _$$_ChatTextResponseCopyWith<$Res>
    implements $ChatTextResponseCopyWith<$Res> {
  factory _$$_ChatTextResponseCopyWith(
          _$_ChatTextResponse value, $Res Function(_$_ChatTextResponse) then) =
      __$$_ChatTextResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? status, MessageResponse? data});

  @override
  $MessageResponseCopyWith<$Res>? get data;
}

/// @nodoc
class __$$_ChatTextResponseCopyWithImpl<$Res>
    extends _$ChatTextResponseCopyWithImpl<$Res, _$_ChatTextResponse>
    implements _$$_ChatTextResponseCopyWith<$Res> {
  __$$_ChatTextResponseCopyWithImpl(
      _$_ChatTextResponse _value, $Res Function(_$_ChatTextResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_ChatTextResponse(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as MessageResponse?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatTextResponse implements _ChatTextResponse {
  _$_ChatTextResponse({this.status, this.data});

  factory _$_ChatTextResponse.fromJson(Map<String, dynamic> json) =>
      _$$_ChatTextResponseFromJson(json);

  @override
  final String? status;
  @override
  final MessageResponse? data;

  @override
  String toString() {
    return 'ChatTextResponse(status: $status, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatTextResponse &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.data, data) || other.data == data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, status, data);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatTextResponseCopyWith<_$_ChatTextResponse> get copyWith =>
      __$$_ChatTextResponseCopyWithImpl<_$_ChatTextResponse>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatTextResponseToJson(
      this,
    );
  }
}

abstract class _ChatTextResponse implements ChatTextResponse {
  factory _ChatTextResponse(
      {final String? status,
      final MessageResponse? data}) = _$_ChatTextResponse;

  factory _ChatTextResponse.fromJson(Map<String, dynamic> json) =
      _$_ChatTextResponse.fromJson;

  @override
  String? get status;
  @override
  MessageResponse? get data;
  @override
  @JsonKey(ignore: true)
  _$$_ChatTextResponseCopyWith<_$_ChatTextResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
