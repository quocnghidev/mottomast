import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mottomast/models/list_message_response/message_response.dart';

part 'chat_text_response.freezed.dart';
part 'chat_text_response.g.dart';

@freezed
class ChatTextResponse with _$ChatTextResponse {
  factory ChatTextResponse({
    String? status,
    MessageResponse? data,
  }) = _ChatTextResponse;

  factory ChatTextResponse.fromJson(Map<String, dynamic> json) =>
      _$ChatTextResponseFromJson(json);
}
