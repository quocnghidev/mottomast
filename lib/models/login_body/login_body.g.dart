// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'login_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_LoginBody _$$_LoginBodyFromJson(Map<String, dynamic> json) => _$_LoginBody(
      email: json['email'] as String?,
      password: json['password'] as String?,
    );

Map<String, dynamic> _$$_LoginBodyToJson(_$_LoginBody instance) =>
    <String, dynamic>{
      'email': instance.email,
      'password': instance.password,
    };
