import 'package:freezed_annotation/freezed_annotation.dart';

part 'login_body.freezed.dart';
part 'login_body.g.dart';

@freezed
class LoginBody with _$LoginBody {
  factory LoginBody({
    String? email,
    String? password,
  }) = _LoginBody;

  factory LoginBody.fromJson(Map<String, dynamic> json) =>
      _$LoginBodyFromJson(json);
}
