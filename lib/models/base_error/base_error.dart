import 'package:freezed_annotation/freezed_annotation.dart';

part 'base_error.freezed.dart';
part 'base_error.g.dart';

@freezed
class BaseError with _$BaseError {
  factory BaseError({
    String? status,
    String? message,
    String? error,
  }) = _BaseError;

  factory BaseError.fromJson(Map<String, dynamic> json) =>
      _$BaseErrorFromJson(json);
}
