// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'base_error.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

BaseError _$BaseErrorFromJson(Map<String, dynamic> json) {
  return _BaseError.fromJson(json);
}

/// @nodoc
mixin _$BaseError {
  String? get status => throw _privateConstructorUsedError;
  String? get message => throw _privateConstructorUsedError;
  String? get error => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $BaseErrorCopyWith<BaseError> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $BaseErrorCopyWith<$Res> {
  factory $BaseErrorCopyWith(BaseError value, $Res Function(BaseError) then) =
      _$BaseErrorCopyWithImpl<$Res, BaseError>;
  @useResult
  $Res call({String? status, String? message, String? error});
}

/// @nodoc
class _$BaseErrorCopyWithImpl<$Res, $Val extends BaseError>
    implements $BaseErrorCopyWith<$Res> {
  _$BaseErrorCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? message = freezed,
    Object? error = freezed,
  }) {
    return _then(_value.copyWith(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_BaseErrorCopyWith<$Res> implements $BaseErrorCopyWith<$Res> {
  factory _$$_BaseErrorCopyWith(
          _$_BaseError value, $Res Function(_$_BaseError) then) =
      __$$_BaseErrorCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? status, String? message, String? error});
}

/// @nodoc
class __$$_BaseErrorCopyWithImpl<$Res>
    extends _$BaseErrorCopyWithImpl<$Res, _$_BaseError>
    implements _$$_BaseErrorCopyWith<$Res> {
  __$$_BaseErrorCopyWithImpl(
      _$_BaseError _value, $Res Function(_$_BaseError) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? message = freezed,
    Object? error = freezed,
  }) {
    return _then(_$_BaseError(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      message: freezed == message
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String?,
      error: freezed == error
          ? _value.error
          : error // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_BaseError implements _BaseError {
  _$_BaseError({this.status, this.message, this.error});

  factory _$_BaseError.fromJson(Map<String, dynamic> json) =>
      _$$_BaseErrorFromJson(json);

  @override
  final String? status;
  @override
  final String? message;
  @override
  final String? error;

  @override
  String toString() {
    return 'BaseError(status: $status, message: $message, error: $error)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_BaseError &&
            (identical(other.status, status) || other.status == status) &&
            (identical(other.message, message) || other.message == message) &&
            (identical(other.error, error) || other.error == error));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, status, message, error);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_BaseErrorCopyWith<_$_BaseError> get copyWith =>
      __$$_BaseErrorCopyWithImpl<_$_BaseError>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_BaseErrorToJson(
      this,
    );
  }
}

abstract class _BaseError implements BaseError {
  factory _BaseError(
      {final String? status,
      final String? message,
      final String? error}) = _$_BaseError;

  factory _BaseError.fromJson(Map<String, dynamic> json) =
      _$_BaseError.fromJson;

  @override
  String? get status;
  @override
  String? get message;
  @override
  String? get error;
  @override
  @JsonKey(ignore: true)
  _$$_BaseErrorCopyWith<_$_BaseError> get copyWith =>
      throw _privateConstructorUsedError;
}
