import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mottomast/models/list_message_response/message_response.dart';

part 'data.freezed.dart';
part 'data.g.dart';

@freezed
class Data with _$Data {
  factory Data({
    List<MessageResponse>? data,
  }) = _Data;

  factory Data.fromJson(Map<String, dynamic> json) => _$DataFromJson(json);
}
