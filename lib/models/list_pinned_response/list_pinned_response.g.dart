// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'list_pinned_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ListPinnedResponse _$$_ListPinnedResponseFromJson(
        Map<String, dynamic> json) =>
    _$_ListPinnedResponse(
      status: json['status'] as String?,
      data: (json['data'] as List<dynamic>?)
          ?.map((e) => MessageResponse.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$$_ListPinnedResponseToJson(
        _$_ListPinnedResponse instance) =>
    <String, dynamic>{
      'status': instance.status,
      'data': instance.data,
    };
