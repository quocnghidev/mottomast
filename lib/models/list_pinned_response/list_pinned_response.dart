import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mottomast/models/list_message_response/message_response.dart';

part 'list_pinned_response.freezed.dart';
part 'list_pinned_response.g.dart';

@freezed
class ListPinnedResponse with _$ListPinnedResponse {
  factory ListPinnedResponse({
    String? status,
    List<MessageResponse>? data,
  }) = _ListPinnedResponse;

  factory ListPinnedResponse.fromJson(Map<String, dynamic> json) =>
      _$ListPinnedResponseFromJson(json);
}
