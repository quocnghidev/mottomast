// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'list_pinned_response.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ListPinnedResponse _$ListPinnedResponseFromJson(Map<String, dynamic> json) {
  return _ListPinnedResponse.fromJson(json);
}

/// @nodoc
mixin _$ListPinnedResponse {
  String? get status => throw _privateConstructorUsedError;
  List<MessageResponse>? get data => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ListPinnedResponseCopyWith<ListPinnedResponse> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ListPinnedResponseCopyWith<$Res> {
  factory $ListPinnedResponseCopyWith(
          ListPinnedResponse value, $Res Function(ListPinnedResponse) then) =
      _$ListPinnedResponseCopyWithImpl<$Res, ListPinnedResponse>;
  @useResult
  $Res call({String? status, List<MessageResponse>? data});
}

/// @nodoc
class _$ListPinnedResponseCopyWithImpl<$Res, $Val extends ListPinnedResponse>
    implements $ListPinnedResponseCopyWith<$Res> {
  _$ListPinnedResponseCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_value.copyWith(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value.data
          : data // ignore: cast_nullable_to_non_nullable
              as List<MessageResponse>?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ListPinnedResponseCopyWith<$Res>
    implements $ListPinnedResponseCopyWith<$Res> {
  factory _$$_ListPinnedResponseCopyWith(_$_ListPinnedResponse value,
          $Res Function(_$_ListPinnedResponse) then) =
      __$$_ListPinnedResponseCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? status, List<MessageResponse>? data});
}

/// @nodoc
class __$$_ListPinnedResponseCopyWithImpl<$Res>
    extends _$ListPinnedResponseCopyWithImpl<$Res, _$_ListPinnedResponse>
    implements _$$_ListPinnedResponseCopyWith<$Res> {
  __$$_ListPinnedResponseCopyWithImpl(
      _$_ListPinnedResponse _value, $Res Function(_$_ListPinnedResponse) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? status = freezed,
    Object? data = freezed,
  }) {
    return _then(_$_ListPinnedResponse(
      status: freezed == status
          ? _value.status
          : status // ignore: cast_nullable_to_non_nullable
              as String?,
      data: freezed == data
          ? _value._data
          : data // ignore: cast_nullable_to_non_nullable
              as List<MessageResponse>?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ListPinnedResponse implements _ListPinnedResponse {
  _$_ListPinnedResponse({this.status, final List<MessageResponse>? data})
      : _data = data;

  factory _$_ListPinnedResponse.fromJson(Map<String, dynamic> json) =>
      _$$_ListPinnedResponseFromJson(json);

  @override
  final String? status;
  final List<MessageResponse>? _data;
  @override
  List<MessageResponse>? get data {
    final value = _data;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  String toString() {
    return 'ListPinnedResponse(status: $status, data: $data)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ListPinnedResponse &&
            (identical(other.status, status) || other.status == status) &&
            const DeepCollectionEquality().equals(other._data, _data));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, status, const DeepCollectionEquality().hash(_data));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ListPinnedResponseCopyWith<_$_ListPinnedResponse> get copyWith =>
      __$$_ListPinnedResponseCopyWithImpl<_$_ListPinnedResponse>(
          this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ListPinnedResponseToJson(
      this,
    );
  }
}

abstract class _ListPinnedResponse implements ListPinnedResponse {
  factory _ListPinnedResponse(
      {final String? status,
      final List<MessageResponse>? data}) = _$_ListPinnedResponse;

  factory _ListPinnedResponse.fromJson(Map<String, dynamic> json) =
      _$_ListPinnedResponse.fromJson;

  @override
  String? get status;
  @override
  List<MessageResponse>? get data;
  @override
  @JsonKey(ignore: true)
  _$$_ListPinnedResponseCopyWith<_$_ListPinnedResponse> get copyWith =>
      throw _privateConstructorUsedError;
}
