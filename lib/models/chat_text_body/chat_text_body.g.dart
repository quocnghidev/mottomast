// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_text_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_ChatTextBody _$$_ChatTextBodyFromJson(Map<String, dynamic> json) =>
    _$_ChatTextBody(
      room: json['room'] as int?,
      content: json['content'] as String?,
    );

Map<String, dynamic> _$$_ChatTextBodyToJson(_$_ChatTextBody instance) =>
    <String, dynamic>{
      'room': instance.room,
      'content': instance.content,
    };
