import 'package:freezed_annotation/freezed_annotation.dart';

part 'chat_text_body.freezed.dart';
part 'chat_text_body.g.dart';

@freezed
class ChatTextBody with _$ChatTextBody {
  factory ChatTextBody({
    int? room,
    String? content,
  }) = _ChatTextBody;

  factory ChatTextBody.fromJson(Map<String, dynamic> json) =>
      _$ChatTextBodyFromJson(json);
}
