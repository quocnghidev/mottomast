// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'chat_text_body.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

ChatTextBody _$ChatTextBodyFromJson(Map<String, dynamic> json) {
  return _ChatTextBody.fromJson(json);
}

/// @nodoc
mixin _$ChatTextBody {
  int? get room => throw _privateConstructorUsedError;
  String? get content => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $ChatTextBodyCopyWith<ChatTextBody> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $ChatTextBodyCopyWith<$Res> {
  factory $ChatTextBodyCopyWith(
          ChatTextBody value, $Res Function(ChatTextBody) then) =
      _$ChatTextBodyCopyWithImpl<$Res, ChatTextBody>;
  @useResult
  $Res call({int? room, String? content});
}

/// @nodoc
class _$ChatTextBodyCopyWithImpl<$Res, $Val extends ChatTextBody>
    implements $ChatTextBodyCopyWith<$Res> {
  _$ChatTextBodyCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? room = freezed,
    Object? content = freezed,
  }) {
    return _then(_value.copyWith(
      room: freezed == room
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as int?,
      content: freezed == content
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_ChatTextBodyCopyWith<$Res>
    implements $ChatTextBodyCopyWith<$Res> {
  factory _$$_ChatTextBodyCopyWith(
          _$_ChatTextBody value, $Res Function(_$_ChatTextBody) then) =
      __$$_ChatTextBodyCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int? room, String? content});
}

/// @nodoc
class __$$_ChatTextBodyCopyWithImpl<$Res>
    extends _$ChatTextBodyCopyWithImpl<$Res, _$_ChatTextBody>
    implements _$$_ChatTextBodyCopyWith<$Res> {
  __$$_ChatTextBodyCopyWithImpl(
      _$_ChatTextBody _value, $Res Function(_$_ChatTextBody) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? room = freezed,
    Object? content = freezed,
  }) {
    return _then(_$_ChatTextBody(
      room: freezed == room
          ? _value.room
          : room // ignore: cast_nullable_to_non_nullable
              as int?,
      content: freezed == content
          ? _value.content
          : content // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_ChatTextBody implements _ChatTextBody {
  _$_ChatTextBody({this.room, this.content});

  factory _$_ChatTextBody.fromJson(Map<String, dynamic> json) =>
      _$$_ChatTextBodyFromJson(json);

  @override
  final int? room;
  @override
  final String? content;

  @override
  String toString() {
    return 'ChatTextBody(room: $room, content: $content)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_ChatTextBody &&
            (identical(other.room, room) || other.room == room) &&
            (identical(other.content, content) || other.content == content));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(runtimeType, room, content);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_ChatTextBodyCopyWith<_$_ChatTextBody> get copyWith =>
      __$$_ChatTextBodyCopyWithImpl<_$_ChatTextBody>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_ChatTextBodyToJson(
      this,
    );
  }
}

abstract class _ChatTextBody implements ChatTextBody {
  factory _ChatTextBody({final int? room, final String? content}) =
      _$_ChatTextBody;

  factory _ChatTextBody.fromJson(Map<String, dynamic> json) =
      _$_ChatTextBody.fromJson;

  @override
  int? get room;
  @override
  String? get content;
  @override
  @JsonKey(ignore: true)
  _$$_ChatTextBodyCopyWith<_$_ChatTextBody> get copyWith =>
      throw _privateConstructorUsedError;
}
