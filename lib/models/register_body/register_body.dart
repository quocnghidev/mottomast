import 'package:freezed_annotation/freezed_annotation.dart';

part 'register_body.freezed.dart';
part 'register_body.g.dart';

@freezed
class RegisterBody with _$RegisterBody {
  factory RegisterBody({
    String? fullName,
    String? email,
    String? password,
  }) = _RegisterBody;

  factory RegisterBody.fromJson(Map<String, dynamic> json) =>
      _$RegisterBodyFromJson(json);
}
