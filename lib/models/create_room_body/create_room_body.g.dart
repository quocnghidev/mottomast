// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'create_room_body.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

_$_CreateRoomBody _$$_CreateRoomBodyFromJson(Map<String, dynamic> json) =>
    _$_CreateRoomBody(
      targets:
          (json['targets'] as List<dynamic>?)?.map((e) => e as int).toList(),
      name: json['name'] as String?,
    );

Map<String, dynamic> _$$_CreateRoomBodyToJson(_$_CreateRoomBody instance) =>
    <String, dynamic>{
      'targets': instance.targets,
      'name': instance.name,
    };
