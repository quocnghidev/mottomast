// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'create_room_body.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

CreateRoomBody _$CreateRoomBodyFromJson(Map<String, dynamic> json) {
  return _CreateRoomBody.fromJson(json);
}

/// @nodoc
mixin _$CreateRoomBody {
  List<int>? get targets => throw _privateConstructorUsedError;
  String? get name => throw _privateConstructorUsedError;

  Map<String, dynamic> toJson() => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  $CreateRoomBodyCopyWith<CreateRoomBody> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $CreateRoomBodyCopyWith<$Res> {
  factory $CreateRoomBodyCopyWith(
          CreateRoomBody value, $Res Function(CreateRoomBody) then) =
      _$CreateRoomBodyCopyWithImpl<$Res, CreateRoomBody>;
  @useResult
  $Res call({List<int>? targets, String? name});
}

/// @nodoc
class _$CreateRoomBodyCopyWithImpl<$Res, $Val extends CreateRoomBody>
    implements $CreateRoomBodyCopyWith<$Res> {
  _$CreateRoomBodyCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? targets = freezed,
    Object? name = freezed,
  }) {
    return _then(_value.copyWith(
      targets: freezed == targets
          ? _value.targets
          : targets // ignore: cast_nullable_to_non_nullable
              as List<int>?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$_CreateRoomBodyCopyWith<$Res>
    implements $CreateRoomBodyCopyWith<$Res> {
  factory _$$_CreateRoomBodyCopyWith(
          _$_CreateRoomBody value, $Res Function(_$_CreateRoomBody) then) =
      __$$_CreateRoomBodyCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({List<int>? targets, String? name});
}

/// @nodoc
class __$$_CreateRoomBodyCopyWithImpl<$Res>
    extends _$CreateRoomBodyCopyWithImpl<$Res, _$_CreateRoomBody>
    implements _$$_CreateRoomBodyCopyWith<$Res> {
  __$$_CreateRoomBodyCopyWithImpl(
      _$_CreateRoomBody _value, $Res Function(_$_CreateRoomBody) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? targets = freezed,
    Object? name = freezed,
  }) {
    return _then(_$_CreateRoomBody(
      targets: freezed == targets
          ? _value._targets
          : targets // ignore: cast_nullable_to_non_nullable
              as List<int>?,
      name: freezed == name
          ? _value.name
          : name // ignore: cast_nullable_to_non_nullable
              as String?,
    ));
  }
}

/// @nodoc
@JsonSerializable()
class _$_CreateRoomBody implements _CreateRoomBody {
  _$_CreateRoomBody({final List<int>? targets, this.name}) : _targets = targets;

  factory _$_CreateRoomBody.fromJson(Map<String, dynamic> json) =>
      _$$_CreateRoomBodyFromJson(json);

  final List<int>? _targets;
  @override
  List<int>? get targets {
    final value = _targets;
    if (value == null) return null;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(value);
  }

  @override
  final String? name;

  @override
  String toString() {
    return 'CreateRoomBody(targets: $targets, name: $name)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$_CreateRoomBody &&
            const DeepCollectionEquality().equals(other._targets, _targets) &&
            (identical(other.name, name) || other.name == name));
  }

  @JsonKey(ignore: true)
  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(_targets), name);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$_CreateRoomBodyCopyWith<_$_CreateRoomBody> get copyWith =>
      __$$_CreateRoomBodyCopyWithImpl<_$_CreateRoomBody>(this, _$identity);

  @override
  Map<String, dynamic> toJson() {
    return _$$_CreateRoomBodyToJson(
      this,
    );
  }
}

abstract class _CreateRoomBody implements CreateRoomBody {
  factory _CreateRoomBody({final List<int>? targets, final String? name}) =
      _$_CreateRoomBody;

  factory _CreateRoomBody.fromJson(Map<String, dynamic> json) =
      _$_CreateRoomBody.fromJson;

  @override
  List<int>? get targets;
  @override
  String? get name;
  @override
  @JsonKey(ignore: true)
  _$$_CreateRoomBodyCopyWith<_$_CreateRoomBody> get copyWith =>
      throw _privateConstructorUsedError;
}
