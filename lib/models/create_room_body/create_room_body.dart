import 'package:freezed_annotation/freezed_annotation.dart';

part 'create_room_body.freezed.dart';
part 'create_room_body.g.dart';

@freezed
class CreateRoomBody with _$CreateRoomBody {
  factory CreateRoomBody({
    List<int>? targets,
    String? name,
  }) = _CreateRoomBody;

  factory CreateRoomBody.fromJson(Map<String, dynamic> json) =>
      _$CreateRoomBodyFromJson(json);
}
