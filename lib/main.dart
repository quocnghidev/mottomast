import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:mottomast/app_binding.dart';
import 'package:mottomast/lang/translation_service.dart';
import 'package:mottomast/routes/app_pages.dart';
import 'package:mottomast/routes/routes.dart';
import 'package:mottomast/theme/app_colors.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setSystemUIOverlayStyle(
    const SystemUiOverlayStyle(
      systemNavigationBarColor: AppColors.statusColor,
      statusBarColor: AppColors.statusColor,
      systemStatusBarContrastEnforced: true,
      systemNavigationBarContrastEnforced: true,
    ),
  );
  await GetStorage.init();
  runApp(
    GetMaterialApp(
      title: 'Mottomast',
      debugShowCheckedModeBanner: false,
      themeMode: ThemeMode.light,
      initialBinding: AppBinding(),
      initialRoute: Routes.SPLASH,
      getPages: AppPages.routes,
      locale: const Locale('vi', 'VN'),
      fallbackLocale: const Locale('vi', 'VN'),
      supportedLocales: const [
        Locale('vi', 'VN'),
        Locale('en', 'US'),
      ],
      defaultTransition: Transition.cupertino,
      localizationsDelegates: GlobalMaterialLocalizations.delegates,
      translations: TranslationService(),
      theme: ThemeData(
        colorScheme: const ColorScheme.light().copyWith(
          primary: AppColors.secondaryColor,
          secondary: AppColors.secondaryColor,
        ),
      ),
    ),
  );
}
