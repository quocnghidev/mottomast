// ignore_for_file: constant_identifier_names

const Map<String, String> vi_VN = {
  'loading_data': 'Đang tải dữ liệu..',
  'login': 'Đăng nhập',
  'create_account': 'Tạo tài khoản',
  'password': 'Mật khẩu',
  'motto': 'Liên lạc cùng một nơi,\ntruy cập ở mọi nơi.',
  'dont_have_account': 'Bạn chưa có tài khoản?',
  'create_now': 'Đăng ký ngay!',
  'full_name': 'Họ tên đầy đủ',
  'confirm_password': 'Xác nhận mật khẩu',
  'already_have_account': 'Bạn đã có tài khoản?',
  'login_now': 'Đăng nhập ngay!',
};
