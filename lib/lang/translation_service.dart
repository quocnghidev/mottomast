import 'package:get/get.dart';
import 'package:mottomast/lang/en_us.dart';
import 'package:mottomast/lang/vi_vn.dart';

class TranslationService extends Translations {
  @override
  Map<String, Map<String, String>> get keys => {
        'en_US': en_US,
        'vi_VN': vi_VN,
      };
}
