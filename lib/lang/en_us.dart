// ignore_for_file: constant_identifier_names

const Map<String, String> en_US = {
  'loading_data': 'Loading data..',
  'login': 'Login',
  'create_account': 'Create account',
  'password': 'Password',
  'motto':
      'All team communication in one place,\nsearchable and accessible anywhere.',
  'dont_have_account': 'Don\'t have account?',
  'create_now': 'Create now!',
  'full_name': 'Full name',
  'confirm_password': 'Confirm password',
  'already_have_account': 'Already have account?',
  'login_now': 'Login now!',
};
