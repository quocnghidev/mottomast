import 'package:get/get.dart';
import 'package:mottomast/api/api_provider.dart';
import 'package:mottomast/api/api_repository.dart';

class AppBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ApiProvider(), permanent: true);
    Get.put(ApiRepository(apiProvider: Get.find()), permanent: true);
  }
}
